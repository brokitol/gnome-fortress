# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/14 12:10:52 by aeddi             #+#    #+#              #
#    Updated: 2015/11/02 17:23:22 by bgauci           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	GnomeFortress

CC			=	clang++
RMF			=	rm -rf
OS			:=	$(shell uname -s)

IFLAGS		=	-I $(INCS_DIR) -I $(INCS_DIR)/Moteur -I $(INCS_DIR)/IHM -I $(LIBS_DIR)/include
CFLAGS		= 	-Wall -Werror -Wextra -std=c++11 $(IFLAGS) 
LFLAGS		=	-lpthread -L $(LIBS_DIR)/lib -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lsfgui 
LFLAGS		+=	-Wl,-rpath,"$(PWD)/$(LIBS_DIR)/lib"

SRCS_DIR	=	sources
OBJS_DIR	=	objects
INCS_DIR	=	includes
LIBS_DIR	=	libs
TMP_DIR		=	.tmp

SRCS		=	main.cpp						\
				debug.cpp						\
				Case.cpp						\
				Data.cpp						\
				Update.cpp						\
				Controlleur.cpp					\
				Position.cpp					\
				Moteur/Creature_Vivante.cpp		\
				Moteur/Entite_Mouvante.cpp		\
				Moteur/Gnome.cpp				\
				Moteur/Humanoide.cpp			\
				Moteur/Liste_Taches.cpp			\
				Moteur/Objet.cpp				\
				Moteur/Path.cpp					\
				Moteur/Pathfinding.cpp			\
				Moteur/Node.cpp					\
				Moteur/Recette.cpp				\
				Moteur/Moteur.cpp				\
				Moteur/Tache.cpp				\
				Moteur/Generator.cpp			\
				IHM/Ihm.cpp						\
				IHM/Ihm_config.cpp				\
				IHM/Ihm_gestion_souris.cpp		\
				IHM/Vertex_Array.cpp			\
				IHM/Gestionnaire_Textures.cpp	\
				IHM/Entite_Affichable.cpp		\
				IHM/Gnome_display.cpp			\
				IHM/Keyboard.cpp				\
				IHM/Mouse.cpp

OBJS		=	$(patsubst %.cpp, $(OBJS_DIR)/%.o, $(SRCS))

ifeq ($(OS), Linux)
	EXT		=	so
else
	EXT		=	dylib
endif

SFGUI_FILES	=	libsfgui.$(EXT)

SFGUI		=	$(patsubst %, $(LIBS_DIR)/lib/%, $(SFGUI_FILES))

SFML_FILES	=	libsfml-audio.$(EXT)		\
				libsfml-graphics.$(EXT)		\
				libsfml-network.$(EXT)		\
				libsfml-system.$(EXT)		\
				libsfml-window.$(EXT)

SFML		=	$(patsubst %, $(LIBS_DIR)/lib/%, $(SFML_FILES))

O			?=	0
OPT_0		=
OPT_1		=	-O3
OPT			=	$(OPT_$(O))

G			?=	0
GDB_0		=
GDB_1		=	-g3 -DDEBUG
GDB			=	$(GDB_$(G))

COL_ON		=	\033[30;47m
COL_OFF		=	\033[0m

all			:	$(NAME)
	@printf "$(COL_ON) Building $(NAME) done $(COL_OFF)\n"

$(NAME)		:	depend $(SFML) $(SFGUI) $(OBJS_DIR) $(OBJS)
	@printf "$(COL_ON) Compiling $(NAME) $(COL_OFF)\n"
	@$(CC) -o $(NAME) $(OBJS) $(GDB) $(OPT) $(CFLAGS) $(LFLAGS) 

$(SFML)		:
	@printf "$(COL_ON) Building SFML $(COL_OFF)\n"
	@mkdir -p $(TMP_DIR)
	@mkdir -p $(LIBS_DIR)
	@if [ ! -d $(TMP_DIR)/SFML ]; then										\
		git clone https://github.com/SFML/SFML.git $(TMP_DIR)/SFML ||		\
		rm -rf $(TMP_DIR)/SFML;												\
	fi
	@if [ -d $(TMP_DIR)/SFML ]; then										\
		(cd $(TMP_DIR)/SFML;												\
		cmake .																\
		-DCMAKE_C_FLAGS="$(OPT) $(GDB)" -DCMAKE_CXX_FLAGS="$(OPT) $(GDB)"	\
		-DCMAKE_INSTALL_PREFIX="$(PWD)/$(LIBS_DIR)"							\
		-DBUILD_SHARED_LIBS="YES"											\
		-DCMAKE_INSTALL_FRAMEWORK_PREFIX="$(PWD)/$(LIBS_DIR)/Frameworks"	\
		-DSFML_BUILD_FRAMEWORKS="NO" &&										\
		cd -) &&															\
		make -C $(TMP_DIR)/SFML install;									\
	fi

$(SFGUI)	:
	@printf "$(COL_ON) Building SGUI $(COL_OFF)\n"
	@mkdir -p $(TMP_DIR)
	@if [ ! -d $(TMP_DIR)/SFGUI ]; then										\
		git clone https://github.com/TankOs/SFGUI.git $(TMP_DIR)/SFGUI ||	\
		rm -rf $(TMP_DIR)/SFGUI;											\
	fi
	@if [ -d $(TMP_DIR)/SFGUI ]; then										\
		(cd $(TMP_DIR)/SFGUI;												\
		cmake .																\
		-DCMAKE_C_FLAGS="$(OPT) $(GDB)" -DCMAKE_CXX_FLAGS="$(OPT) $(GDB)"	\
		-DSFGUI_BUILD_EXAMPLES="NO"											\
		-DSFML_ROOT="$(PWD)/$(LIBS_DIR)"									\
		-DCMAKE_INSTALL_PREFIX="$(PWD)/$(LIBS_DIR)"							\
		-DCMAKE_MACOSX_RPATH="YES" &&										\
		cd -) &&															\
		make -C $(TMP_DIR)/SFGUI install;									\
	fi

$(OBJS_DIR)/%.o	:	$(addprefix $(SRCS_DIR)/, %.cpp)
	@printf "$(COL_ON) Building object: $@ $(COL_OFF)\n"
	@$(CC) -o $@ $(CFLAGS) $(GDB) $(OPT) -c $<

$(OBJS_DIR)	:
	@mkdir -p $(OBJS_DIR)
	@mkdir -p $(OBJS_DIR)/Moteur
	@mkdir -p $(OBJS_DIR)/IHM

dlclean		:
	@printf "$(COL_ON) Cleaning download folders $(COL_OFF)\n"
	@rm -rf $(TMP_DIR)

clean		:
	@printf "$(COL_ON) Cleaning build folders $(COL_OFF)\n"
	@rm -rf $(OBJS_DIR)

fclean		:	clean
	@printf "$(COL_ON) Cleaning compiled targets $(COL_OFF)\n"
	rm -rf $(LIBS_DIR)
	@rm -rf $(NAME)

re			:	clean all
re_all		:	fclean all

depend		:
	@$(RMF) Makefile.bak
	@ #makedepend -Y -pobjects $(IFLAGS) $(addprefix $(SRCS_DIR)/, $(SRCS)) 2>/dev/null

.PHONY:	clean fclean re depend all
