/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controlleur.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:21:43 by bgauci            #+#    #+#             */
/*   Updated: 2015/07/03 18:39:39 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTROLLEUR_HPP
#define CONTROLLEUR_HPP

class Controlleur;

#include "Moteur.hpp"
#include "Ihm.hpp"

class Controlleur
{
	private :
		Moteur	moteur;
		Ihm *	ihm;

	public :
		Controlleur();

		void	main();
		void	pause();
		void	load();
		void	save();

		void	add_tache_chaise();
		void	add_tache_chaise(Position const &p);
};

#endif
