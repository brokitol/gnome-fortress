#ifndef DATA_HPP
#define DATA_HPP

#include "Case.hpp"
#include <Update.hpp>
#include <vector>

//
// monde[z][x][y]
//
class Data
{
	private :
		bool												pause = false;
		int													hauteur = 0;
		int													largeur = 0;
		int													longeur = 0;
		std::vector< std::vector< std::vector< Case > > >	*monde;	//	monde[hauteur][largeur][longeur]

	public :
		Data(std::vector< std::vector< std::vector< Case > > > *monde, int hauteur, int largeur, int longeur);

	public :
		int													get_hauteur();
		int													get_largeur();
		int													get_longeur();
		bool												get_pause();
		std::vector< std::vector< std::vector< Case > > >	*get_monde();
		Case												&get_case(Position const &pos) const;
		Case												&get_case(int x, int y, int z) const;


	public :
		void												set_pause(bool p);
};

#endif
