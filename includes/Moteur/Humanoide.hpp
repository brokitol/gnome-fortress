/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   humanoide.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:31:09 by bgauci            #+#    #+#             */
/*   Updated: 2015/07/02 18:22:12 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANOIDE_HPP
#define HUMANOIDE_HPP

#include "Creature_Vivante.hpp"

class Humanoide : public Creature_Vivante
{
};

#endif
