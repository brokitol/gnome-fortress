#ifndef GENERATOR_HPP
#define GENERATOR_HPP

#include "Data.hpp"
#include "Entite_Mouvante.hpp"

// Map generator

class Generator
{

private:
	Data		&data;
	Generator(void);
	
public:
	// Constructors
	Generator(Generator const &src);
	Generator(Data &data);

	// Destructor
	~Generator(void);
	
	// Accessors
	Data							&get_data(void) const;
		
	void							set_data(Data &data);
	
	// Custom methods
	bool		map_add_hole(Position const &pos, int radius, int depth);
	bool		map_add_height(Position const &pos, int radius, int height);
	bool		map_add_rmountain(Position const &pos, int radius, int depth, int smooth = 1, int cut = 0);
	bool		map_add_mountain(Position const &pos, int radius, int height, int smooth = 1, int cut = 0);

	void		generate_map(int base_radius, int base_smooth, unsigned int seed);
	void		generate_entities(std::list< std::shared_ptr< Entite_Mouvante > > &entities);

	// Operator overloads
	Generator						&operator=(Generator const &rhs);
};

#endif
