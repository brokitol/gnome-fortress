/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Gnome.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:30:27 by bgauci            #+#    #+#             */
/*   Updated: 2015/11/01 17:15:38 by vinz             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GNOME_HPP
#define GNOME_HPP

#include "Humanoide.hpp"
#include "Tache.hpp"

#include <memory>

class Gnome : public Humanoide
{
	private:
		Position		move_to;
		Tache			task;
		bool			is_task;
		int				search_item; // 1 eau, 2 nourriture
		unsigned long int		search_item_id;
		std::string		name;
	public:
		Gnome( void );
		Gnome( std::string name );
//		Gnome( Position position );
//		Gnome( Position position, std::weak_ptr<Gnome> this_ptr);
		~Gnome( void );
		Gnome( Gnome const & src );

		std::string		get_name(void) const;
		void			set_name(std::string const name);

		Tache			get_tache(void) const;
		bool			get_is_task(void) const;
		int				get_search_item(void) const;
		Position		get_move_to(void) const;

		Gnome & operator=( Gnome const & rhs );

		void	nouveau_tour(void);
		void	init(Position position, std::weak_ptr<Gnome> this_ptr);
		void	exec_tache( void );
		bool	go_to_objet(std::string obj, int type);
		void	conso_search_objet( void );
};

#endif
