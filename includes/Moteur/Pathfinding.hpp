/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pathfinding.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 14:10:30 by bgauci            #+#    #+#             */
/*   Updated: 2015/07/13 14:10:34 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PATHFINDING_HPP
#define PATHFINDING_HPP

#include "Position.hpp"
#include "Path.hpp"
#include "Entite_Mouvante.hpp"


class Pathfinding
{
	public :
		static Path	get_path(const Position depart, const Position arriver, Entite_Mouvante &entite);

	private :
		static Path get_path_a_star(Position const depart, Position const arriver, Entite_Mouvante &entite);
};

#endif
