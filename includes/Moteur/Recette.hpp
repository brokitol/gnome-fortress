
#ifndef RECETTE_HPP
# define RECETTE_HPP

//# include <ostream>
# include <string>
# include <list>
# include "Position.hpp"
# include "Objet.hpp"

class Recette
{

private:
	std::string							unique_id;
	std::string							name;
	int									type_action;
	std::list<std::string>				req_items_list;
	std::string							result_item; //Unique id

	static std::list<Recette>			recettes_base;

protected:

public:
	// Constructors
	Recette(void);
	Recette(Recette const &src);
	Recette(std::string unique_id, std::string name, int type_action, std::list<std::string> req_items_list, std::string result_item);
	
	// Destructor
	~Recette(void);
	
	// Accessors
	std::string				getUnique_id(void) const;
	std::string				getName(void) const;
	int						getType_action(void) const;
	std::list<std::string>	getReq_items_list(void) const;
	std::string				getResult_item(void) const;

	void				setUnique_id(std::string unique_id);	
	void				setName(std::string name);
	void				setType_action(int type_action);
	void				setReq_items_list(std::list<std::string> req_items_list);
	void				setResult_item(std::string result_item);

	bool				prepare(Position &target);
	bool				execute(Position &target);
	bool				execute_and_spawn(Position &target, Objet &obj);


	static void			chargerRecettesBase(void);
	static Recette		getRecetteBaseByUniqueId(std::string unique_id);

	// Operator overloads
	Recette				&operator=(Recette const &rhs);
};

// std::ostream				&operator<<(std::ostream &stream, Recette const &item);

#endif
