/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Path.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:44:04 by bgauci            #+#    #+#             */
/*   Updated: 2015/07/15 03:53:12 by vinz             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PATH_HPP
#define PATH_HPP

#include "Position.hpp"
#include <list>
# include <iostream>
class Path
{
	private :
		std::list<Position>				lst;
		std::list<Position>::iterator	actuel = lst.begin();

	private :

	public :
		Path() {}
		Path(Path const &src);
		Path(std::list<Position> lst);
		~Path();

	public :
		Path &	operator=(Path const &rhs);

	public :
		Position	next();
		Position	last() const;
		Position	front() const;
		Position	back() const;
		int			get_path_size( void );
};

#endif
