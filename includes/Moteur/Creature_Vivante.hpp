/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Creature_Vivante.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:31:49 by bgauci            #+#    #+#             */
/*   Updated: 2015/11/01 17:01:16 by vinz             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CREATURE_VIVANTE_HPP
#define CREATURE_VIVANTE_HPP

#include "Entite_Mouvante.hpp"

class Creature_Vivante : public Entite_Mouvante
{
	private:
		int	vie;
		int	fatigue;
		int	soif;
		int faim;

	public:
		Creature_Vivante( void );
		~Creature_Vivante( void );
		Creature_Vivante( Creature_Vivante const & src );
		Creature_Vivante & operator=( Creature_Vivante const & rhs );

		void	set_vie(int vie);
		void	set_fatigue(int fatigue);
		void	set_soif(int soif);
		void	set_faim(int faim);

		int		get_vie( void ) const;
		int		get_fatigue( void ) const;
		int		get_soif( void ) const;
		int		get_faim( void ) const;

		void 	dec_stats( void );
		void 	die( void );
};

#endif
