// by DamDam

#ifndef TACHE_HPP
# define TACHE_HPP

//# include <ostream>
# include <string>
# include "Position.hpp"
# include "Recette.hpp"

class Tache
{

private:
	static unsigned long int	unique_id_current;
	unsigned long int			unique_id;
	std::string					action;
	Position					target;
	std::string					recette; //unique_id
	bool						locked = false;

protected:

public:
	// Constructors
	Tache(void);
	Tache(Tache const &src);
	Tache(std::string action, Position target, std::string recette);
	
	// Destructor
	~Tache(void);
	
	// Accessors
	std::string				getAction(void) const;
	Position				getTarget(void) const;
	std::string				getRecette(void) const;
	unsigned long int		getUniqueId(void) const;

	void				setAction(std::string action);
	void				setTarget(Position target);
	void				setRecette(std::string recette);
	void				setUniqueId(unsigned long int unique_id);
	void				setLocked(bool const locked);
	bool				getLocked(void) const;

	void				execute(void);

	
	// Operator overloads
	Tache				&operator=(Tache const &rhs);
};

//std::ostream				&operator<<(std::ostream &stream, Tache const &item);

#endif
