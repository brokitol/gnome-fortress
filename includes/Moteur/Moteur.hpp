/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Moteur.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/30 16:25:51 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/15 17:16:50 by vinz             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MOTEUR_HPP
#define MOTEUR_HPP

#include "Entite_Mouvante.hpp"
#include "Case.hpp"
#include "Liste_Taches.hpp"
#include "Data.hpp"
#include "Generator.hpp"

#include <vector>
#include <memory>
#include <thread>

//
// monde[z][x][y]
//

class Moteur
{
	private :
		bool												end = false;
		std::list< std::shared_ptr< Entite_Mouvante > >		lst_entite;
		std::list< std::shared_ptr< Objet > >				lst_obj;
		Liste_Taches										lst_tache;
		std::thread											*boucle;
		Data												*data;

	public :
		Moteur();
		~Moteur(void);

		Moteur &								operator+=(std::shared_ptr<Entite_Mouvante> obj);
		Moteur &								operator-=(std::shared_ptr<Entite_Mouvante> obj);
		Moteur &								operator+=(std::shared_ptr<Objet> obj);
		Moteur &								operator-=(std::shared_ptr<Objet> obj);

		void									set_end(bool val);

		bool									get_end(void);
		Case &									get_case(Position const &pos);
		Data *									get_data();
		Liste_Taches *							get_liste_tache( void );
		std::list< std::shared_ptr< Objet > > *	get_liste_obj( void );
		std::shared_ptr< Objet >				get_obj( unsigned long int id );

		Data *		nouveau_jeu_custom(int largeur, int longueur, int hauteur, int base_radius, int base_smooth, unsigned int seed);

		void		run(void);
		void		charger_jeu();
		void		sauvegarder_jeu();
		std::string	to_s();

		void		add_tache_chaise(Position const &p);
		void		add_tache_chaise();
		void		add_chaise(Position const &pos);
		void		add_table(Position const &pos);
		void		add_pomme(Position const &pos);
		void		add_gourde(Position const &pos);
		void		add_tache(std::string nom_action, Position target, std::string num_recette);

	private :
		void		boucle_de_jeu();
};

extern Moteur	*the_moteur;

#endif
