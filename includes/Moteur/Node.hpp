#ifndef NODE_HPP
#define NODE_HPP

#include "Position.hpp"

#include <cstdlib> // => NULL

class Node
{
	private :
		Position	p;
		int			heuristique = 9999;
		int			cost = 0;
		Node *		precedente = NULL;

	public :
		Node();
		Node(Position const p, Position const arriver, Node *precedente = NULL);
		Node(Node const & src);

		Node &		operator=(Node const & rhs);
		bool		operator==(Node const & rhs) const;
		bool		operator==(Position const & rhs) const;
		bool		operator!=(Node const & rhs) const;
		bool		operator!=(Position const & rhs) const;
		bool		operator<(Node const & rhs) const;
		bool		operator>(Node const & rhs) const;

		int			get_heuristique() const ;
		int			get_cost() const ;
		int			get_val() const ;
		Node *		get_precedente() const ;
		Position	get_position() const ;

		static int	calc_heuristique(Position const depart, Position const arriver);
};


#endif
