/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Entite_Mouvante.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:34:27 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/18 11:42:00 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef ENTITE_MOUVANTE_HPP
#define ENTITE_MOUVANTE_HPP

class Entite_Mouvante;

#include "Entite_Affichable.hpp"
#include "Position.hpp"
#include "Path.hpp"
#include "Case.hpp"
#include <unistd.h>
#include <memory>

class Entite_Mouvante : public Entite_Affichable
{
	private:
		
	public:
		Entite_Mouvante( void );
		Entite_Mouvante( Position position );
		~Entite_Mouvante( void );
		Entite_Mouvante( Entite_Mouvante const & src );
		Entite_Mouvante & operator=( Entite_Mouvante const & rhs );

		virtual void	nouveau_tour(void);
		void			ret_path(Position pos);
		bool			is_accessible(Position pos);
		void			move();
		Position		get_test_pos( void ); // test
		Position		get_pos( void );
		bool			traverse(Case const &c) const;
		void			move_to_pos(Position pos);
	protected:
		Path		path;
		Position	pos;
		std::weak_ptr<Entite_Mouvante> w_ptr;
};

#endif
