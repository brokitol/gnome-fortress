
#ifndef LISTE_TACHES_HPP
# define LISTE_TACHES_HPP

//# include <ostream>
//# include <string>
# include <list>
# include "Tache.hpp"

class Liste_Taches
{

private:
	std::list<Tache>				taches;
	unsigned long int				current_id = 0;

protected:

public:
	// Constructors
	Liste_Taches(void);
	Liste_Taches(Liste_Taches const &src);
	
	// Destructor
	~Liste_Taches(void);
	
	// Accessors
	std::list<Tache>	getTaches(void) const;

	
	void				setTaches(std::list<Tache> taches);

	void				addTache(Tache tache);
	void				removeTache(unsigned long int unique_id);

	Tache 				getFreeTache( void );
	bool				isFreeTache( void );

	void				lockTache(unsigned long int unique_id);

	// Operator overloads
	Liste_Taches				&operator=(Liste_Taches const &rhs);
};

// std::ostream				&operator<<(std::ostream &stream, Liste_Taches const &item);

#endif
