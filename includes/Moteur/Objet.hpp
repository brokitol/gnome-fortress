
#ifndef OBJET_HPP
# define OBJET_HPP

//# include <ostream>
# include <string>
# include <exception>
# include <memory>
# include <list>
# include "Position.hpp"
# include "Entite_Affichable.hpp"

class Objet : public Entite_Affichable
{

private:
	unsigned long int		id; // id UNIQUE !
	std::string					unique_id = "NA";
	std::string					nom = "default";
	int							valeur_monetaire = 0;
	bool						locked = false;
	Position					position;

	static std::list<Objet>		objets_base;

protected:

public:
	// Constructors
	Objet(void);
	Objet(Objet const &src);

	Objet(std::string unique_id);
	Objet(std::string unique_id, std::string nom, int valeur_monetaire);
	Objet(std::string unique_id, std::string nom, int image_id, int valeur_monetaire);
	static unsigned long int	autoincrement;
	// Destructor
	~Objet(void);

	// Accessors
	unsigned long int			get_obj_id( void ) const;
	std::string						getUnique_id(void) const;
	std::string						getNom(void) const;
	int								getValeur_monetaire(void) const;
	bool							getLocked(void) const;
	Position						getPosition(void) const;

	void							setUnique_id(std::string const unique_id);
	void							setNom(std::string const nom);
	void							setValeur_monetaire(int const valeur_monetaire);
	void							setLocked(bool const locked);
	void							setPosition(Position const position);

	static void						parseFile(std::string filename) throw(std::exception);

	static void						spawn(std::shared_ptr<Objet> obj, Position const &target);
	static void						copy_and_spawn(Objet const &obj, Position const &target);


	static void						chargerObjetsBase(void);
	static Objet					getObjetBaseByUniqueId(std::string unique_id);

	// Operator overloads
	Objet					&operator=(Objet const &rhs);
};

// std::ostream				&operator<<(std::ostream &stream, Objet const &item);

#endif
