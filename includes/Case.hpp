/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Case.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/02 16:04:02 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/02 11:57:28 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CASE_HPP
#define CASE_HPP

class Case;

#include "Entite_Mouvante.hpp"
#include "Objet.hpp"
#include <Update.hpp>
#include <vector>
#include <list>
#include <memory>

class Case
{
	private :
		std::shared_ptr<Objet>							sol = NULL;
		std::shared_ptr<Objet>							contenu = NULL;
		std::list<std::shared_ptr<Entite_Mouvante>>		occupant;
		std::list<std::shared_ptr<Objet>>				divers;
		Update											update;

	public :
		Case(void);
		Case(std::shared_ptr<Objet> sol, std::shared_ptr<Objet> contenu);

	public :
		Case &											operator+=(std::shared_ptr<Entite_Mouvante> obj);
		Case &											operator-=(std::shared_ptr<Entite_Mouvante> obj);
		Case &											operator+=(std::shared_ptr<Objet> obj);
		Case &											operator-=(std::shared_ptr<Objet> obj);
		void											set_sol(std::shared_ptr<Objet> obj);
		void											set_contenu(std::shared_ptr<Objet> obj);
		std::shared_ptr<Objet>							get_sol(void) const;
		std::shared_ptr<Objet>							get_contenu(void) const;
		std::list<std::shared_ptr<Entite_Mouvante>>		&get_occupant(void);
		std::list<std::shared_ptr<Objet>>				&get_divers(void);
		Update											&get_update(void);

	public :
		bool	peut_monter(std::shared_ptr<Entite_Mouvante> const & toto) const;
		bool	peut_descendre(std::shared_ptr<Entite_Mouvante> const & toto) const;
		bool	est_traversable(Entite_Mouvante const & toto) const;
};

#endif
