/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Update.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/01 16:51:51 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 12:02:08 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UPDATE_HPP
# define UPDATE_HPP

class Update
{
	public:
		Update(void);
		~Update(void);

		void	set_floor_flag(void);
		void	set_content_flag(void);
		void	set_object_flag(void);
		void	set_character_flag(void);

		bool	is_floor_updated(void);
		bool	is_content_updated(void);
		bool	is_object_updated(void);
		bool	is_character_updated(void);

		bool	operator==(bool compare);

		void	reset_flags(void);

	private:
		bool	_global;
		bool	_floor;
		bool	_content;
		bool	_object;
		bool	_character;
};

#endif /* !UPDATE_HPP */
