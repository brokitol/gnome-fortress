/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Gnome_display.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/18 21:51:33 by plastic           #+#    #+#             */
/*   Updated: 2015/11/12 22:41:53 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GNOME_DISPLAY_HPP
# define GNOME_DISPLAY_HPP

# include <Gnome.hpp>
# include <SFML/Graphics.hpp>
# include <SFGUI/SFGUI.hpp>
# include <SFGUI/Widgets.hpp>

class Gnome_display : public sfg::Window
{
	public:
		Gnome_display(sfg::Desktop *desktop, std::shared_ptr<Gnome> gnome, const sf::FloatRect &pos);
		~Gnome_display(void);

		static void	callback_button_list_case(sfg::Desktop *desktop, std::shared_ptr<Gnome> gnome, const sf::FloatRect &pos);

		sfg::Desktop					*_desktop;
		std::shared_ptr<Gnome>			gnome;
		std::shared_ptr<Gnome_display>	sp;
		std::shared_ptr<sfg::Box>		box_stat = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
		std::shared_ptr<sfg::Box>		box;
	private:

		void							actualise(void);
		void							Refresh();
};

void	destroy_win(std::shared_ptr<Gnome_display> gd);

#endif /* !GNOME_DISPLAY_HPP */
