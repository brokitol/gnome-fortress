/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ihm.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/22 23:07:06 by plastic           #+#    #+#             */
/*   Updated: 2015/11/02 17:31:09 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IHM_HPP
# define IHM_HPP

class Ihm;

# include <SFML/Graphics.hpp>
# include <SFGUI/SFGUI.hpp>
# include <SFGUI/Widgets.hpp>
# include <Keyboard.hpp>
# include <Mouse.hpp>
# include <Vertex_Array.hpp>
# include <Data.hpp>
# include <vector>
# include "Controlleur.hpp"

class Ihm
{
	public:
		Ihm(Controlleur *controlleur);
		~Ihm();

		void	start_menu(void);
		int		main_loop(Data *data);

		Controlleur			*_controlleur;

	private:
		Position	click_souris(sf::Event event);
		Position	Perforation_click(Position p);
		void		actu_fenetre_click(Position p, sf::Event event);
		void		fenetre_click_gauche(Position p, sf::Event event);
		void		fenetre_click_droite(Position p, sf::Event event);
		void		quitter_fenetre_click(void);
		bool		click_sur_un_menu(int x, int y) const;
		void		add_tache(void);
		void		_setup_gui(void);
		void		_pause_toggle(void);
		void		_get_framerate(void);
		void		_display_map(void);

		sf::RenderWindow				*_window;
		sf::View						_camera;
		std::shared_ptr<sfg::Window>	fenetre_click = NULL;
		Position						last_click;

		Keyboard			_keyboard;
		Mouse				_mouse;

		sf::Clock			_clock;
		sf::Font			_font;

		sfg::SFGUI			_sfgui;
		sfg::Desktop		_desktop;

		bool				_run;

		Data 				*_data;
		Vertex_Array		*_vertex_array;

	public : // pour le menu de creation de terrain
		static	std::map<std::string, int>	menu_generation_terrain();
		static	void						OnValideClick(bool * valide);
};

#endif /* !IHM_HPP */
