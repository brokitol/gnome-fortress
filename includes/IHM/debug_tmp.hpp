/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/07 11:28:37 by aeddi             #+#    #+#             */
/*   Updated: 2015/09/02 16:20:53 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEBUG_TMP_HPP
# define DEBUG_TMP_HPP

# ifdef DEBUG

#  include <iostream>
#  include <iomanip>

#  define COL_WIDTH		15
#  define COL_START		std::setw(COL_WIDTH) << std::left  
#  define COL_START_F	std::setw(2) << "| " << std::setw(COL_WIDTH) << std::left  
#  define COL_END		std::setw(2) << "| "  
#  define COL_END_F		std::setw(3) << "|| "  
#  define LIN_WIDTH		((COL_WIDTH + 2) * 5) + 1
#  define B_TO_MB(b)	static_cast<float>(b / (1024. * 1024.))


# endif /* !DEBUG */

#endif /* !DEBUG_HPP */
