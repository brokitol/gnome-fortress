/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Keyboard.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/21 19:43:36 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 00:01:58 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYBOARD_HPP
# define KEYBOARD_HPP

# include <SFML/Graphics.hpp>
# include <Vertex_Array.hpp>

class Keyboard
{
	public :
		Keyboard();
		~Keyboard();

		void	update(sf::View &camera, Vertex_Array &vertex_array, sf::Event event);

	private:
		bool	_moveLeft;
		bool	_moveUp;
		bool	_moveRight;
		bool	_moveDown;
		bool	_display_more;
		bool	_display_less;

		void	_update_move(sf::Event event);
		void	_make_move(sf::View &camera);

		void	_update_layer_display(sf::Event event, Vertex_Array &vertex_array);
};

#endif /* !KEYBOARD_HPP */
