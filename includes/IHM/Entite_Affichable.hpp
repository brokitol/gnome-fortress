/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Entite_Affichable.hpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/16 21:30:11 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 01:17:37 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENTITE_AFFICHABLE_HPP
# define ENTITE_AFFICHABLE_HPP

# include <SFML/Graphics.hpp>
# include <Gestionnaire_Textures.hpp>
# include <Position.hpp>

# define OFFSET_XY_HALF		16
# define OFFSET_XY_QUART	8
# define OFFSET_Z			16

class Entite_Affichable
{
	public :
		Entite_Affichable();
		Entite_Affichable(int index_text);
		Entite_Affichable(int index_text, int x, int y, int z);
		~Entite_Affichable();

		void			set_sprite(int index, int x, int y, int z);
		void			set_sprite(int index, Position const &pos);

		void			set_index_text(int index_text);
		int				get_index_text(void) const;

		void			set_sprite_position(int x, int y, int z);
		sf::Vector3i	get_sprite_position(void);

		sf::Vector2f	get_offset_text(void);

	private:
		void			_update_sprite(void);

	protected:
		int				_index_text;
		sf::Vector3i	_position;
		sf::Vector2f	_offset;
};

#endif /* !ENTITE_AFFICHABLE_HPP */
