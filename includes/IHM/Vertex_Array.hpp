/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Vertex_Array.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/30 14:26:37 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 13:50:58 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VERTEX_ARRAY_HPP
# define VERTEX_ARRAY_HPP

# include <SFML/Graphics.hpp>
# include <Case.hpp>

# define OBJECT_AMOUNT	3
# define BATCH_MAX_SIZE	1000000

class Vertex_Array : public sf::Drawable, public sf::Transformable
{
	public:
		Vertex_Array(unsigned int height, unsigned int width, unsigned int depth);
		~Vertex_Array(void);

		void			update_floor(Case &to_update, sf::Vector3<unsigned int> position);
		void			update_content(Case &to_update, sf::Vector3<unsigned int> position);
		void			update_object(Case &to_update, sf::Vector3<unsigned int> position);
		void			update_character(Case &to_update, sf::Vector3<unsigned int> position);

		void			display_more(void);
		void			display_less(void);

		unsigned int	get_layers_displayed(void) const;

	private:
		sf::Vertex	*_get_quad(short sublayer, sf::Vector3<unsigned int> position);
		void		_fill_quad(sf::Vertex *quad, sf::Vector2f &offset, sf::Rect<int> &area);
		void		_reset_quad(sf::Vertex *quad);

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		unsigned int	_layer_width;
		unsigned int	_layer_size;
		unsigned int	_sub_layer_size;
		unsigned int	_layers_amount;
		unsigned int	_layers_displayed;
		unsigned int	_vertices_displayed;
		sf::VertexArray	_vertices;
};

#endif /* !VERTEX_ARRAY_HPP */
