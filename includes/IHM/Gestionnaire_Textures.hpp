/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Gestionnaire_Textures.hpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/16 21:36:37 by plastic           #+#    #+#             */
/*   Updated: 2015/09/01 20:53:56 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GESTIONNAIRE_TEXTURES_HPP
# define GESTIONNAIRE_TEXTURES_HPP

# include <SFML/Graphics.hpp>
# include <vector>

class Gestionnaire_Textures
{
	public:
		Gestionnaire_Textures();
		~Gestionnaire_Textures();

		sf::Texture&	get_texture(void);
		sf::Rect<int>	get_area(int index);
		sf::Vector2f	get_offset(int index);

	private:
		void _set_sub_texture(const sf::Rect<int> &area, const sf::Vector2f offset);

		sf::Texture					_texture;
		std::vector<sf::Rect<int>>	_areas;
		std::vector<sf::Vector2f>	_offsets;
};

#endif /* !GESTIONNAIRE_TEXTURES_HPP */
