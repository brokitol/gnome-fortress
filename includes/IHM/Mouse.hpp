/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Mouse.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/21 20:23:52 by plastic           #+#    #+#             */
/*   Updated: 2015/09/01 20:54:46 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MOUSE_HPP
# define MOUSE_HPP

# include <SFML/Graphics.hpp>

class Mouse
{
	public:
		Mouse();
		~Mouse();

		void	update(sf::View &camera, sf::Event event);

	private:
		bool			_move;
		sf::Vector2i	_oldPos;

		void	_update_move(sf::Event event);
		void	_make_move(sf::Event event, sf::View &camera);

		void	_update_zoom(sf::Event event, sf::View &camera);
};

#endif /* !MOUSE_HPP */
