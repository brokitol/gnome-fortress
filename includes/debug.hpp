// by DamDam

#ifndef DEBUG_HPP
# define DEBUG_HPP

# include <string>

void	debug(std::string text);

/*# define DEBUG(num, text) {								\
							if (DEBUG_LEVEL >= num)		\
							{							\
								debug(text);			\
							}							\
						}*/

# define PDEBUG(num, text) if (DEBUG_LEVEL >= num) debug(text);

#endif
