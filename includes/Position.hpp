/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   position.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:41:52 by bgauci            #+#    #+#             */
/*   Updated: 2015/07/02 18:23:26 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef POSITION_HPP
#define POSITION_HPP

class Position
{
	private:
		int	x = 0; // largeur
		int	y = 0; // longeur
		int	z = 0; // hauteur
		int	angle = 0;
	public:
		Position( void );
		Position( int x, int y, int z, int angle);
		Position( int x, int y, int z);
		Position( Position const & src );
		~Position( void );
		Position &	operator=( Position const & rhs );

		bool		operator==( Position const & rhs ) const;
		bool		operator!=( Position const & rhs ) const;

		int			get_x() const;
		int			get_y() const;
		int			get_z() const;
		int			get_angle() const;

		void			set_x(int n);
		void			set_y(int n);
		void			set_z(int n);
		void			set_angle(int n);
};

#endif
