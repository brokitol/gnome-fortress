/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Update.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/02 11:44:55 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 12:21:04 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Update.hpp>

Update::Update(void)
: _global(false), _floor(false), _content(false), _object(false), _character(false) {}

Update::~Update(void) {}

void	Update::set_floor_flag(void)
{
	this->_global = true;
	this->_floor = true;
}

void	Update::set_content_flag(void)
{
	this->_global = true;
	this->_content = true;
}

void	Update::set_object_flag(void)
{
	this->_global = true;
	this->_object = true;
}

void	Update::set_character_flag(void)
{
	this->_global = true;
	this->_character = true;
}

bool	Update::is_floor_updated(void)
{
	return this->_floor;
}

bool	Update::is_content_updated(void)
{
	return this->_content;
}

bool	Update::is_object_updated(void)
{
	return this->_object;
}

bool	Update::is_character_updated(void)
{
	return this->_character;
}

bool	Update::operator==(bool compare)
{
	return (this->_global == compare);
}

void	Update::reset_flags(void)
{
	this->_global = false;
	this->_floor = false;
	this->_content = false;
	this->_object = false;
	this->_character = false;
}
