/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   position.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/30 16:36:52 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/02 12:14:02 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Position.hpp"


Position::Position( void ) : x(0), y(0), z(0), angle(0) {}

Position::Position(int x, int y, int z, int angle) : x(x), y(y), z(z), angle(angle) {}

Position::Position(int x, int y, int z) : x(x), y(y), z(z), angle(0) {}

Position::Position( Position const & src ) { *this = src; }

Position::~Position() {}

Position & Position::operator=( Position const & rhs )
{
	this->x = rhs.x;
	this->y = rhs.y;
	this->z = rhs.z;
	this->angle = rhs.angle;

	return *this;
}

bool		Position::operator==( Position const & rhs ) const
{
	if (this->x != rhs.x)
		return false;
	if (this->y != rhs.y)
		return false;
	if (this->z != rhs.z)
		return false;
//	if (this->angle != rhs.angle)
//		return false;
	return true;
}

bool		Position::operator!=( Position const & rhs ) const {return !(*this == rhs);}

int			Position::get_x() const {return this->x;}
int			Position::get_y() const {return this->y;}
int			Position::get_z() const {return this->z;}
int			Position::get_angle() const {return this->angle;}

void		Position::set_x(int n) {this->x = n;}
void		Position::set_y(int n) {this->y = n;}
void		Position::set_z(int n) {this->z = n;}
void		Position::set_angle(int n) {this->angle = n;}
