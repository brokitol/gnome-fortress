#include "Data.hpp"
#include "debug.hpp"

#define DEBUG_LEVEL 0

Data::Data(std::vector< std::vector< std::vector< Case > > > *monde, int hauteur, int largeur, int longeur) : hauteur(hauteur), largeur(largeur), longeur(longeur) {this->monde = monde;}
int													Data::get_hauteur() {return hauteur;}
int													Data::get_largeur() {return largeur;}
int													Data::get_longeur() {return longeur;}
bool												Data::get_pause() {return pause;}

std::vector< std::vector< std::vector< Case > > > * Data::get_monde() {return monde;}

void												Data::set_pause(bool p) {this->pause = p;}

Case &												Data::get_case(Position const &pos) const
{
	return get_case(pos.get_x(), pos.get_y(), pos.get_z());
}

Case &												Data::get_case(const int x, const int y, const int z) const
{
	if (x >= this->largeur || x < 0)
	{
		std::cout << "Data-get_case : hors map, x : " << std::to_string(x) << std::endl;
		throw std::exception();
	}
	if (y >= this->longeur || y < 0)
	{
		std::cout << "Data-get_case : hors map, y : " << std::to_string(y) << std::endl;
		throw std::exception();
	}
	if (z >= this->hauteur || z < 0)
	{
		std::cout << "Data-get_case : hors map, z : " << std::to_string(z) << std::endl;
		throw std::exception();
	}
	return (*monde)[z][x][y];
}
