/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controlleur.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:21:14 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/02 17:44:01 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Controlleur.hpp"
#include "Moteur.hpp"
#include <debug.hpp>

#define DEBUG_LEVEL 3

Moteur	*the_moteur = NULL;

Controlleur::Controlleur() {ihm = new Ihm(this);}

void	Controlleur::main(void)
{
	the_moteur = &(this->moteur);

	// normalement affichage des menus pour le choix entre créer un nouveau monde ou en charger un.
	// ihm->startMenu();
	std::map<std::string, int>	config = Ihm::menu_generation_terrain();

  	PDEBUG(3, "Controleur : creation jeu");
  	
  	// hauteur, largeur, longueur, rayon de base, lissage de base, seed du rand()
	auto data = this->moteur.nouveau_jeu_custom(config["hauteur"], config["largeur"], config["longeur"], config["rayon"], config["lissage"], config["seed"]);
	
  	PDEBUG(3, "Controleur : lancement run");
	this->moteur.run();
  	PDEBUG(3, "Controleur : run fini");
	// le monde est créé, l'IHM peut obtenir toute les infos requise pour l'afficher
  	PDEBUG(3, "Controleur : ihm main loop debut");
	ihm->main_loop(data);
  	PDEBUG(3, "Controleur : ihm main loop fini");

	this->moteur.set_end(true);
	sleep(1);

	return ;
}

void	Controlleur::add_tache_chaise() {this->moteur.add_tache_chaise();}
void	Controlleur::add_tache_chaise(Position const &p) {this->moteur.add_tache_chaise(p);}
