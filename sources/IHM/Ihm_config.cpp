/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ihm_config.cpp	                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/09 19:37:46 by plastic           #+#    #+#             */
/*   Updated: 2015/11/02 17:20:03 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Ihm.hpp>
#include <string>

std::map<std::string, int>	Ihm::menu_generation_terrain()
{
	std::map<std::string, int>	config;
	bool						valide = false;
	sfg::SFGUI					m_sfgui;

	sf::RenderWindow render_window(sf::VideoMode(800, 600), "Creation de monde" );						// Create SFML's window.

	auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );								// Create a vertical box layouter with 5 pixels spacing 

	box->Pack( sfg::Label::Create( "hauteur" ) );			auto hauteur	= sfg::Entry::Create("20");	box->Pack( hauteur, false );
	box->Pack( sfg::Label::Create( "largeur" ) );			auto largeur	= sfg::Entry::Create("50");	box->Pack( largeur, false );
	box->Pack( sfg::Label::Create( "longeur" ) );			auto longeur	= sfg::Entry::Create("50");	box->Pack( longeur, false );
	box->Pack( sfg::Label::Create( "rayon de base" ) );		auto rayon		= sfg::Entry::Create("5");	box->Pack( rayon, false );
	box->Pack( sfg::Label::Create( "lissage de base" ) );	auto lissage	= sfg::Entry::Create("1");	box->Pack( lissage, false );
	box->Pack( sfg::Label::Create( "seed (0 = random)" ) );	auto seed		= sfg::Entry::Create("0");	box->Pack( seed, false );

	auto button = sfg::Button::Create( "valider" );
	button->GetSignal( sfg::Widget::OnLeftClick ).Connect( std::bind( &Ihm::OnValideClick, &valide ) );
	box->Pack( button, false );

	auto window = sfg::Window::Create();
	window->SetTitle( "titre" );
	window->Add( box );

	sfg::Desktop desktop;
	desktop.Add( window );

	render_window.resetGLStates();

	// Main loop!
	sf::Event event;
	sf::Clock clock;

	while( render_window.isOpen() )
	{
		while( render_window.pollEvent( event ) )
		{
			desktop.HandleEvent( event );
			if( event.type == sf::Event::Closed or valide == true)
				render_window.close();
		}

		desktop.Update( clock.restart().asSeconds() );

		render_window.clear();
		m_sfgui.Display( render_window );
		render_window.display();
	}

	config["hauteur"] = std::stoi(hauteur->GetText().toAnsiString());
	config["largeur"] = std::stoi(largeur->GetText().toAnsiString());
	config["longeur"] = std::stoi(longeur->GetText().toAnsiString());
	config["rayon"] = std::stoi(rayon->GetText().toAnsiString());
	config["lissage"] = std::stoi(lissage->GetText().toAnsiString());
	config["seed"] = std::stoi(seed->GetText().toAnsiString());

	return config;
}
