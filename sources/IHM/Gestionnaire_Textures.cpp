/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Gestionnaire_Textures.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/16 21:53:36 by plastic           #+#    #+#             */
/*   Updated: 2015/11/16 19:39:58 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Gestionnaire_Textures.hpp>

Gestionnaire_Textures::Gestionnaire_Textures()
{
	sf::Image img;

	img.loadFromFile("assets/sprites/sprites.png");
	if (!this->_texture.loadFromImage(img))
		throw "Chargement de la texture impossible impossible";

	this->_set_sub_texture(sf::IntRect(384, 20, 32, 32), sf::Vector2f(0, -16));		// 0 - Dummy block

	// Floors
	this->_set_sub_texture(sf::IntRect(0, 52, 32, 20), sf::Vector2f(0, 0));			// 1 - Stone
	this->_set_sub_texture(sf::IntRect(320, 240, 32, 20), sf::Vector2f(0, 0));		// 2 - Grass

	// Contents
	this->_set_sub_texture(sf::IntRect(0, 72, 32, 32), sf::Vector2f(0, -16));		// 3 - Wall stone
	this->_set_sub_texture(sf::IntRect(0, 260, 32, 32), sf::Vector2f(0, -16));		// 4 - Tree
	this->_set_sub_texture(sf::IntRect(544, 312, 32, 32), sf::Vector2f(0, -16));	// 5 - Gnome
	this->_set_sub_texture(sf::IntRect(128, 0, 32, 20), sf::Vector2f(0, 0));		// 6 - Water

	// Items
	this->_set_sub_texture(sf::IntRect(16, 444, 16, 16), sf::Vector2f(8, 0));		// 7 - Strawberry

	this->_set_sub_texture(sf::IntRect(544, 312, 32, 32), sf::Vector2f(0, -16));	// 8 - Gnome 2
	this->_set_sub_texture(sf::IntRect(0, 639, 32, 32), sf::Vector2f(0, -16));		// 9 - Llama
	this->_set_sub_texture(sf::IntRect(32, 639, 32, 32), sf::Vector2f(0, -16));		// 10 - Gnu
	this->_set_sub_texture(sf::IntRect(0, 257, 32, 32), sf::Vector2f(0, -16));		// 11 - Sapin - Gnome 5
	this->_set_sub_texture(sf::IntRect(384, 283, 32, 32), sf::Vector2f(0, -16));	// 12 - Chaise
	this->_set_sub_texture(sf::IntRect(382, 316, 32, 32), sf::Vector2f(0, -16));	// 13 - Table
	this->_set_sub_texture(sf::IntRect(32, 312, 32, 32), sf::Vector2f(0, -16));		// 14 - Escalier
	this->_set_sub_texture(sf::IntRect(416, 20, 32, 32), sf::Vector2f(0, -16));		// Sprite vide

	this->_set_sub_texture(sf::IntRect(238, 428, 16, 16), sf::Vector2f(5, -3));		// 16 - Diamant

	this->_set_sub_texture(sf::IntRect(64, 639, 32, 32), sf::Vector2f(0, -16));		// 17 - Ostrich (Autruche)
	this->_set_sub_texture(sf::IntRect(0, 607, 32, 32), sf::Vector2f(0, -16));		// 18 - Honey Badger
	this->_set_sub_texture(sf::IntRect(32, 607, 32, 32), sf::Vector2f(0, -16));		// 19 - Lizard
	this->_set_sub_texture(sf::IntRect(64, 607, 32, 32), sf::Vector2f(0, -16));		// 20 - Hippopotamus
	this->_set_sub_texture(sf::IntRect(0, 671, 32, 32), sf::Vector2f(0, -16));		// 21 - Spider


}

Gestionnaire_Textures::~Gestionnaire_Textures() {}

void Gestionnaire_Textures::_set_sub_texture(const sf::Rect<int> &area, const sf::Vector2f offset)
{
	this->_areas.push_back(area);
	this->_offsets.push_back(offset);
}

sf::Texture&	Gestionnaire_Textures::get_texture(void)
{
	return this->_texture;
}

sf::Rect<int>	Gestionnaire_Textures::get_area(int index)
{
	return this->_areas[index];
}

sf::Vector2f	Gestionnaire_Textures::get_offset(int index)
{
	return this->_offsets[index];
}
