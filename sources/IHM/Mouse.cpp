/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Mouse.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/21 20:23:46 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 16:43:52 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Mouse.hpp>
#include <debug_tmp.hpp>

Mouse::Mouse() : _move(false) {}
Mouse::~Mouse() {}

void	Mouse::update(sf::View &camera, sf::Event event)
{
	if (event.type == sf::Event::MouseButtonPressed ||
		event.type == sf::Event::MouseButtonReleased)
		this->_update_move(event);
	else if (event.type == sf::Event::MouseMoved)
		this->_make_move(event, camera);
	else if (event.type == sf::Event::MouseWheelScrolled)
		this->_update_zoom(event, camera);
}

void	Mouse::_update_move(sf::Event event)
{
	if (event.mouseButton.button == sf::Mouse::Left)
	{
		if (event.type == sf::Event::MouseButtonPressed)
		{
			this->_move = true;
			this->_oldPos.x = event.mouseButton.x;
			this->_oldPos.y = event.mouseButton.y;
#ifdef DEBUG
			std::cout << "Mouse button " << event.mouseButton.button << " has been pressed"
			" on position X: " << event.mouseButton.x << " - Y: " << event.mouseButton.y << std::endl << std::endl;
#endif
		}
		if (event.type == sf::Event::MouseButtonReleased)
		{
			this->_move = false;
#ifdef DEBUG
			std::cout << "Mouse button " << event.mouseButton.button << " has been released"
			" on position X: " << event.mouseButton.x << " - Y: " << event.mouseButton.y << std::endl << std::endl;
#endif
		}
	}
}

void	Mouse::_make_move(sf::Event event, sf::View &camera)
{
	int	x;
	int	y;

	if (this->_move)
	{
		x = this->_oldPos.x - event.mouseMove.x;
		y = this->_oldPos.y - event.mouseMove.y;
		camera.move(x, y);
		this->_oldPos.x = event.mouseMove.x;
		this->_oldPos.y = event.mouseMove.y;
	}
}

void	Mouse::_update_zoom(sf::Event event, sf::View &camera)
{
	float zoom;

	zoom = 1. - (event.mouseWheelScroll.delta / 10);
	camera.zoom(zoom);
#ifdef DEBUG
	std::cout << "Mousewheel has been scrolled with a distance of " << zoom << std::endl << std::endl;
#endif
}
