/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Gnome_display.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/18 21:51:21 by plastic           #+#    #+#             */
/*   Updated: 2015/11/12 22:31:26 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Gnome_display.hpp>
#include <Recette.hpp>

Gnome_display::Gnome_display(sfg::Desktop *desktop, std::shared_ptr<Gnome> gnome, const sf::FloatRect &pos)
: Window(Style::TOPLEVEL), _desktop(desktop), gnome(gnome), sp(this)
{
	this->_desktop->Add(sp);

	this->SetTitle("Gnome infos");
	this->SetAllocation(pos);

	box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 15.0f);

	box->Pack(box_stat);
	this->actualise();

	auto quitter = sfg::Button::Create("Fermer");
	quitter->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(destroy_win, sp));
	box->Pack(quitter, false);

	this->Add(box);
}

Gnome_display::~Gnome_display(void) {}

void	Gnome_display::Refresh()
{
	Window::Refresh();
	actualise();
}

void	Gnome_display::actualise()
{
	box_stat->RemoveAll();

	box_stat->Pack(sfg::Label::Create("Nom : " + gnome->get_name()));
	box_stat->Pack(sfg::Label::Create("Vie : " + std::to_string(gnome->get_vie())));
	box_stat->Pack(sfg::Label::Create("Fatigue : " + std::to_string(gnome->get_fatigue())));
	box_stat->Pack(sfg::Label::Create("Soif : " + std::to_string(gnome->get_soif())));
	box_stat->Pack(sfg::Label::Create("Faim : " + std::to_string(gnome->get_faim())));
	box_stat->Pack(sfg::Label::Create(" "));
	box_stat->Pack(sfg::Label::Create("Actuellement : "));

	if (!this->gnome->get_is_task())
	{
		if (this->gnome->get_search_item() == 0)		
			box_stat->Pack(sfg::Label::Create("Se balade"));
		else if (this->gnome->get_search_item() == 1)		
			box_stat->Pack(sfg::Label::Create("Cherche de l eau"));
		else if (this->gnome->get_search_item() == 2)		
			box_stat->Pack(sfg::Label::Create("Cherche de la nourriture"));
	}
	else
	{
		std::string		nom_recette = this->gnome->get_tache().getRecette();

		box_stat->Pack(sfg::Label::Create("Prepare la tache"));
		
		if (nom_recette == "dig")
			box_stat->Pack(sfg::Label::Create("[ Creuser ]"));
		else
		{
			Recette		recette = Recette::getRecetteBaseByUniqueId(nom_recette);

			if (recette.getUnique_id() == "ERROR")
				box_stat->Pack(sfg::Label::Create("[ Se balader ]"));
			else
				box_stat->Pack(sfg::Label::Create("[" + recette.getName() + "]"));
		}
	}


}

void Gnome_display::callback_button_list_case(sfg::Desktop *desktop, std::shared_ptr<Gnome> gnome, const sf::FloatRect &pos)
{
 	new Gnome_display(desktop, gnome, pos);
}

void	destroy_win(std::shared_ptr<Gnome_display> gd)
{
	if (gd->_desktop)
	{
		gd->Show(false);
		gd->_desktop->Remove(gd->sp);
		gd->_desktop = NULL;
	}
}

