/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Keyboard.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/21 18:48:19 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 16:43:54 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Keyboard.hpp>
#include <debug_tmp.hpp>

Keyboard::Keyboard()
: _moveLeft(false), _moveUp(false), _moveRight(false), _moveDown(false), _display_more(true), _display_less(true) {}

Keyboard::~Keyboard() {}

void	Keyboard::update(sf::View &camera, Vertex_Array &vertex_array, sf::Event event)
{

		if (event.type == sf::Event::KeyPressed ||
			event.type == sf::Event::KeyReleased)
		{
			this->_update_move(event);
			this->_update_layer_display(event, vertex_array);
		}
		this->_make_move(camera);
}

void	Keyboard::_update_move(sf::Event event)
{
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Left)
			this->_moveLeft = true;
		else if (event.key.code == sf::Keyboard::Right)
			this->_moveUp = true;
		else if (event.key.code == sf::Keyboard::Up)
			this->_moveRight = true;
		else if (event.key.code == sf::Keyboard::Down)
			this->_moveDown = true;
#ifdef DEBUG
		std::cout << "Key code " << event.key.code << " has been pressed" << std::endl << std::endl;
#endif
	}
	if (event.type == sf::Event::KeyReleased)
	{
		if (event.key.code == sf::Keyboard::Left)
			this->_moveLeft = false;
		else if (event.key.code == sf::Keyboard::Right)
			this->_moveUp = false;
		else if (event.key.code == sf::Keyboard::Up)
			this->_moveRight = false;
		else if (event.key.code == sf::Keyboard::Down)
			this->_moveDown = false;
#ifdef DEBUG
		std::cout << "Key code " << event.key.code << " has been released" << std::endl << std::endl;
#endif
	}
}

void	Keyboard::_make_move(sf::View &camera)
{
	if (this->_moveLeft)
			camera.move(-5., 0.);
	if (this->_moveUp)
			camera.move(5., 0.);
	if (this->_moveRight)
			camera.move(0., -5.);
	if (this->_moveDown)
			camera.move(0., 5.);
}

void	Keyboard::_update_layer_display(sf::Event event, Vertex_Array &vertex_array)
{
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::O && this->_display_more)
		{
			vertex_array.display_more();
			this->_display_more = false;
		}
		if (event.key.code == sf::Keyboard::I && this->_display_less)
		{
			vertex_array.display_less();
			this->_display_less = false;
		}
	}
	if (event.type == sf::Event::KeyReleased)
	{
		if (event.key.code == sf::Keyboard::O)
			this->_display_more = true;
		if (event.key.code == sf::Keyboard::I)
			this->_display_less = true;
	}
}
