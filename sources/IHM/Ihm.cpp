/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp 		                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/09 19:37:46 by plastic           #+#    #+#             */
/*   Updated: 2015/11/12 17:53:43 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Ihm.hpp>
#include <string>

#include "Gnome_display.hpp"

Ihm::Ihm(Controlleur * controlleur) : _controlleur(controlleur)
{
    this->_window = new sf::RenderWindow(sf::VideoMode::getDesktopMode(), "Gnome Fortress proto");
	this->_camera = this->_window->getDefaultView();
	this->_camera.setCenter(0, 0);
#ifndef DEBUG
	this->_window->setFramerateLimit(60);
	this->_window->setVerticalSyncEnabled(true);
#endif
	this->_font.loadFromFile("assets/fonts/PERRYGOT.TTF");
}

Ihm::~Ihm(void)
{
	delete this->_window;
	delete this->_vertex_array;
}

void						Ihm::OnValideClick(bool * valide) {*valide = true;}

int	Ihm::main_loop(Data *data)
{
	sf::Event				event;
	sf::Clock				guiclock;

	this->_data = data;
	this->_vertex_array = new Vertex_Array(data->get_hauteur(), data->get_largeur(), data->get_longeur());
	this->_setup_gui();
    while (this->_window->isOpen())
    {
        while (this->_window->pollEvent(event))
        {
			this->_desktop.HandleEvent(event);
            if (event.type == sf::Event::Closed)
                this->_window->close();
			if (event.type == sf::Event::Resized)
				this->_camera.setSize(event.size.width, event.size.height);
			if (event.type == sf::Event::MouseButtonReleased)
				this->click_souris(event);
			if (data->get_pause() == false)
			{
				this->_keyboard.update(this->_camera, *this->_vertex_array, event);
				this->_mouse.update(this->_camera, event);
			}
        }
		this->_window->clear(sf::Color(0x73, 0xC2, 0xFB, 0xFF));
		this->_window->setView(this->_camera);
		this->_display_map();
		this->_window->setView(this->_window->getDefaultView());
#ifdef DEBUG
		this->_get_framerate();
#endif
		this->_desktop.Update(guiclock.restart().asSeconds());
		this->_sfgui.Display(*this->_window);
		this->_window->display();
    }
    return EXIT_SUCCESS;
}

void	Ihm::add_tache()
{
	this->_controlleur->add_tache_chaise(this->last_click);
}

void	Ihm::_setup_gui(void)
{
	auto button1 = sfg::Button::Create("Pause");
	button1->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&Ihm::_pause_toggle, this));
	auto button2 = sfg::Button::Create("Tache");
	button2->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind((void(Ihm::*)(void))&Ihm::add_tache, this));
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
	box->Pack(button1, false);
	box->Pack(button2, false);

	auto guiwindow = sfg::Window::Create();
	guiwindow->Add(box);
	this->_desktop.Add(guiwindow);
}

void	Ihm::_pause_toggle(void)
{ 
	this->_data->set_pause(!this->_data->get_pause());
}

void	Ihm::_display_map(void)
{
	Case		*current;
	Update		*update;
#ifdef DEBUG
	static bool	first = true;
	bool		header_displayed = false;
#endif

	for (int itz = 0; itz < this->_data->get_hauteur(); itz++)
	{
		for (int itx = 0; itx < this->_data->get_largeur(); itx++)
		{
			for (int ity = 0; ity < this->_data->get_longeur(); ity++)
			{
				current = &this->_data->get_case(Position(itx, ity, itz));
				update = &current->get_update();
				if (*update == true)
				{
#ifdef DEBUG
					if (!first)
					{
						if (!header_displayed)
						{
							std::cout << "Vertex_Array update:" << std::endl << std::string(20, '-') << std::endl;
							header_displayed = true;
						}
						std::cout << "Case X:" << itx << " Y:" << ity << " Z:" << itz << "\t";
					}
#endif
					if (update->is_floor_updated())
					{
#ifdef DEBUG
						if (!first)
							std::cout << " floor";
#endif
						this->_vertex_array->update_floor(*current, sf::Vector3<unsigned int>(itx, ity, itz));
					}
					if (update->is_content_updated())
					{
#ifdef DEBUG
						if (!first)
							std::cout << " content";
#endif
						this->_vertex_array->update_content(*current, sf::Vector3<unsigned int>(itx, ity, itz));
					}
					if (update->is_object_updated())
					{
#ifdef DEBUG
						if (!first)
							std::cout << " object";
#endif
						this->_vertex_array->update_object(*current, sf::Vector3<unsigned int>(itx, ity, itz));
					}
					if (update->is_character_updated())
					{
#ifdef DEBUG
						if (!first)
							std::cout << " character";
#endif
						this->_vertex_array->update_character(*current, sf::Vector3<unsigned int>(itx, ity, itz));
					}
#ifdef DEBUG
						if (!first)
							std::cout << std::endl;
#endif
					update->reset_flags();
				}
			}
		}
	}
	this->_window->draw(*this->_vertex_array);
#ifdef DEBUG
	first = false;
	if (!first && header_displayed)
		std::cout << std::endl;
#endif
	this->_desktop.Refresh();
}

void	Ihm::_get_framerate(void)
{
	sf::Text		text;
	static int		framerate = 0;
	static int		framecount = 0;
	static sf::Time	frametime;

	frametime += this->_clock.restart();
	if (frametime.asSeconds() >= 1.)
	{
		frametime -= sf::seconds(1.);
		framerate = framecount;
		framecount = 0;
	}
	framecount++;

	std::string string = std::string("Framerate ") + std::to_string(framerate) + std::string("/s");

	text.setFont(this->_font);
	text.setString(string);
	text.setCharacterSize(21);
	text.setColor(sf::Color(0, 0, 0, 255));
	text.setPosition(10., 10.);
	this->_window->draw(text);
}
