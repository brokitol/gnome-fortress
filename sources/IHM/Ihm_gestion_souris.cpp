/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp 		                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/09 19:37:46 by plastic           #+#    #+#             */
/*   Updated: 2015/11/02 17:33:31 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Ihm.hpp>
#include <string>

#include "Gnome_display.hpp"

Position	Ihm::click_souris(sf::Event event)
{
	int x = event.mouseButton.x;
	int y = event.mouseButton.y;
	Position p;

	if (click_sur_un_menu(x, y))
		return p;

	std::cout << "click, il y a eu un click !! [" << x << "]/[" << y << "]" << std::endl;
	std::cout << "[" << _camera.getCenter().x << "]/[" << _camera.getCenter().y << "]" << std::endl;
	std::cout << "[" << _camera.getSize().x << "]/[" << _camera.getSize().y << "]" << std::endl;

	x = x - (_camera.getSize().x / 2) + _camera.getCenter().x; // decalage par rapport a la camera
	y = y - (_camera.getSize().y / 2) + _camera.getCenter().y;
	y += (OFFSET_Z * (_data->get_hauteur() - 1)); // decalage pour être sur la plus haute couche de la map

	std::cout << "[" << x << "]/[" << y << "]" << std::endl;

	std::vector<std::vector<std::vector<Case> > > *tmp1 = _data->get_monde();
	std::vector<std::vector<Case> > tmp2 = tmp1->back();

	int res_x = -1;
	int	res_y = -1;
	int dis = 999;

	for (int x2 = 0 ; x2 < (_data->get_largeur() + _data->get_hauteur()) ; x2++)
		for (int y2 = 0 ; y2 < (_data->get_longeur() + _data->get_hauteur()); y2++)
		{
			int a = 0; // x
			int b = 0; // y

			a += OFFSET_XY_HALF * x2;
			b += OFFSET_XY_QUART * x2;

			a -= OFFSET_XY_HALF * y2;
			b += OFFSET_XY_QUART * y2;

			if (x > a && x < a + 32 && y > b && y < b + 20)
			{
				std::cout << "case possible : [" << x2 << "," << y2 << "] ( a:" << a << " b:" << b << ")" << std::endl;
				int dis_tmp = abs(x - (a + OFFSET_XY_HALF)) + abs(y - (b + OFFSET_XY_QUART));
				if (dis_tmp < dis)
				{
					dis = dis_tmp;
					res_x = x2;
					res_y = y2;
				}
			}
		}
	
	std::cout << "case choisi : [" << res_x << "," << res_y << "]" << std::endl;
	
	p = Perforation_click(Position(res_x, res_y, _data->get_hauteur() - 1));

	std::cout << "case final : [" << p.get_x() << "," << p.get_y() << "," << p.get_z() << "]" << std::endl;

	if (!(p.get_x() < 0 || p.get_y() < 0 || p.get_z() < 0))
	{
		this->actu_fenetre_click(p, event);
		this->last_click = p;
	}
	else
		this->quitter_fenetre_click();

	return p;
}

Position Ihm::Perforation_click(Position p)
{
	if (p.get_x() < 0 || p.get_y() < 0 || p.get_z() < 0)
		return p;

	try
	{	
	if ((int)_vertex_array->get_layers_displayed() > p.get_z())
		if (_data->get_case(p).get_sol() != NULL)
			return p;
	}
	catch (std::exception e) {}

	Position tmp = p;
	tmp.set_x(tmp.get_x() - 1);
	tmp.set_y(tmp.get_y() - 1);
	tmp.set_z(tmp.get_z() - 1);
	return Perforation_click(tmp);
}

void	Ihm::actu_fenetre_click(Position p, sf::Event event)
{
	if (this->fenetre_click == NULL)
	{
		this->fenetre_click = sfg::Window::Create();
		this->_desktop.Add(fenetre_click);
	}

	if (event.mouseButton.button != sf::Mouse::Button::Left)
		this->fenetre_click_droite(p, event);
	else
		this->fenetre_click_gauche(p, event);
}

void	Ihm::fenetre_click_droite(Position p, sf::Event event)
{
	int nb_button = 0;
	this->fenetre_click->SetTitle( "case [" + std::to_string(p.get_x()) + ", " + std::to_string(p.get_y()) + ", " + std::to_string(p.get_z()) + "]" );
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

	nb_button++;
	auto bouton_1 = sfg::Button::Create("chaise en bois");
	bouton_1->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&Moteur::add_tache, the_moteur, "chaise en bois", p, "000001"));
	box->Pack(bouton_1, false, false);

	nb_button++;
	auto bouton_2 = sfg::Button::Create("chaise en pierre");
	bouton_2->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&Moteur::add_tache, the_moteur, "chaise en pierre", p, "000002"));
	box->Pack(bouton_2, false, false);

	nb_button++;
	auto bouton_3 = sfg::Button::Create("table en bois");
	bouton_3->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&Moteur::add_tache, the_moteur, "table en bois", p, "000011"));
	box->Pack(bouton_3, false, false);

	nb_button++;
	auto bouton_4 = sfg::Button::Create("gourde d'eau");
	bouton_4->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&Moteur::add_tache, the_moteur, "gourde d'eau", p, "000020"));
	box->Pack(bouton_4, false, false);

	nb_button++;
	auto bouton_5 = sfg::Button::Create("pomme");
	bouton_5->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&Moteur::add_tache, the_moteur, "pomme", p, "000030"));
	box->Pack(bouton_5, false, false);

	nb_button++;
	auto bouton_6 = sfg::Button::Create("dig");
	bouton_6->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&Moteur::add_tache, the_moteur, "dig", p, "dig"));
	box->Pack(bouton_6, false, false);

	this->fenetre_click->RemoveAll();
	this->fenetre_click->Add(box);
	this->fenetre_click->SetAllocation(sf::FloatRect(event.mouseButton.x, event.mouseButton.y, 120, 40 + nb_button * 30));
}

void	Ihm::fenetre_click_gauche(Position p, sf::Event event)
{
	int nb_button = 1, nb_label = 0;
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

	Case &c = _data->get_case(p);
	for (auto tmp : c.get_occupant())
	{
		std::shared_ptr<Gnome> gn = std::dynamic_pointer_cast<Gnome>(tmp);
		if (gn != NULL)
		{
			nb_button++;
			auto bouton_gnome = sfg::Button::Create(gn->get_name());
			bouton_gnome->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&Gnome_display::callback_button_list_case, &_desktop, gn, sf::FloatRect(150,50,200,200)));
			box->Pack(bouton_gnome, false, false);
		}
	}
	auto sol = c.get_sol();
	if (sol != NULL)
	{
		nb_label++;
		box->Pack(sfg::Label::Create(sol->getNom()));
	}
	auto contenu = c.get_contenu();
	if (contenu != NULL)
	{
		nb_label++;
		box->Pack(sfg::Label::Create(contenu->getNom()));
	}
	for (auto tmp : c.get_divers())
	{
		nb_label++;
		box->Pack(sfg::Label::Create(tmp->getNom()));
	}

	this->fenetre_click->RemoveAll();
	this->fenetre_click->Add(box);
	this->fenetre_click->SetAllocation(sf::FloatRect(event.mouseButton.x - 120, event.mouseButton.y, 120, 40 + nb_button * 30 + nb_label * 20));
}

void	Ihm::quitter_fenetre_click(void)
{
	if (this->fenetre_click != NULL)
	{
		this->_desktop.Remove(fenetre_click);
		this->fenetre_click = NULL;
	}
}

bool	Ihm::click_sur_un_menu(int x, int y) const
{
	if (fenetre_click != NULL)
	{
		sf::FloatRect fc = fenetre_click->GetAllocation();
		if (x > fc.left and x < (fc.left + fc.width) and y > fc.top and y < (fc.top + fc.height))
			return true;
	}

	return false;
}
