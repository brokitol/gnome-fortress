/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Vertex_Arrays.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/30 14:26:24 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 17:00:27 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Vertex_Array.hpp>
#include <debug_tmp.hpp>

Gestionnaire_Textures g_textures;

Vertex_Array::Vertex_Array(unsigned int height, unsigned int width, unsigned int depth)
: _layer_width(width), _layers_amount(height), _layers_displayed(height)
{
	this->_sub_layer_size = width * depth * 4;
	this->_layer_size = this->_sub_layer_size * (3 + OBJECT_AMOUNT);
	this->_vertices_displayed = this->_layers_displayed * this->_layer_size;
	this->_vertices.setPrimitiveType(sf::Quads);
	this->_vertices.resize(this->_layers_amount * this->_layer_size);

#ifdef DEBUG
	unsigned int	Vertex_Array_size;
	unsigned int	batch_amount;

	Vertex_Array_size = this->_layers_amount * this->_layer_size;
	batch_amount = this->_vertices_displayed / BATCH_MAX_SIZE;
	batch_amount += (this->_vertices_displayed % BATCH_MAX_SIZE) ? 1 : 0;

	std::cout << "Map of size " << height << " x " << width << " x " << depth << " with "
	<< OBJECT_AMOUNT << " object layers" << std::endl

	<< std::endl << std::string(LIN_WIDTH, '-') << std::endl

	<< COL_START_F	<< "Data / Units"	<< COL_END_F
	<< COL_START	<< "Vertices" 		<< COL_END
	<< COL_START	<< "Quads"			<< COL_END
	<< COL_START	<< "Bytes"			<< COL_END
	<< COL_START	<< "Megabytes"		<< COL_END

	<< std::endl << std::string(LIN_WIDTH, '=') << std::endl

	<< COL_START_F	<< "Vertex array"										<< COL_END_F
	<< COL_START	<< Vertex_Array_size									<< COL_END
	<< COL_START	<< Vertex_Array_size / 4								<< COL_END
	<< COL_START	<< Vertex_Array_size * sizeof(sf::Vertex)				<< COL_END
	<< COL_START	<< B_TO_MB(Vertex_Array_size * sizeof(sf::Vertex))		<< COL_END

	<< std::endl << std::string(LIN_WIDTH, '-') << std::endl

	<< COL_START_F	<< "Layer"												<< COL_END_F
	<< COL_START	<< this->_layer_size									<< COL_END
	<< COL_START	<< this->_layer_size / 4								<< COL_END
	<< COL_START	<< this->_layer_size * sizeof(sf::Vertex)				<< COL_END
	<< COL_START	<< B_TO_MB(this->_layer_size * sizeof(sf::Vertex))		<< COL_END

	<< std::endl << std::string(LIN_WIDTH, '-') << std::endl

	<< COL_START_F	<< "Sublayer"											<< COL_END_F
	<< COL_START	<< this->_sub_layer_size								<< COL_END
	<< COL_START	<< this->_sub_layer_size / 4							<< COL_END
	<< COL_START	<< this->_sub_layer_size * sizeof(sf::Vertex)			<< COL_END
	<< COL_START	<< B_TO_MB(this->_sub_layer_size * sizeof(sf::Vertex))	<< COL_END

	<< std::endl << std::string(LIN_WIDTH, '-') << std::endl

	<< COL_START_F	<< "Batch"												<< COL_END_F
	<< COL_START	<< BATCH_MAX_SIZE										<< COL_END
	<< COL_START	<< BATCH_MAX_SIZE / 4									<< COL_END
	<< COL_START	<< BATCH_MAX_SIZE * sizeof(sf::Vertex)					<< COL_END
	<< COL_START	<< B_TO_MB(BATCH_MAX_SIZE * sizeof(sf::Vertex)) 		<< COL_END

	<< std::endl << std::string(LIN_WIDTH, '-') << std::endl

	<< std::endl << batch_amount << " batchs needed to display entire map" << std::endl << std::endl;
#endif
}

Vertex_Array::~Vertex_Array(void) {}

void		Vertex_Array::update_floor(Case &to_update, sf::Vector3<unsigned int> position)
{
	std::shared_ptr<Objet>	floor;
	sf::Vector2f			offset;
	sf::Rect<int>			area;

	floor = to_update.get_sol();
	if (floor)
	{
		offset = floor->get_offset_text();
		offset += g_textures.get_offset(floor->get_index_text());
		area = g_textures.get_area(floor->get_index_text());

		this->_fill_quad(this->_get_quad(0, position), offset, area);
	}
	else
		this->_reset_quad(this->_get_quad(0, position));
}

void		Vertex_Array::update_content(Case &to_update, sf::Vector3<unsigned int> position)
{
	std::shared_ptr<Objet>	content;
	sf::Vector2f			offset;
	sf::Rect<int>			area;

	content = to_update.get_contenu();
	if (content)
	{
		offset = content->get_offset_text();
		offset += g_textures.get_offset(content->get_index_text());
		area = g_textures.get_area(content->get_index_text());

		this->_fill_quad(this->_get_quad(1, position), offset, area);
	}
	else
		this->_reset_quad(this->_get_quad(1, position));
}

void		Vertex_Array::update_object(Case &to_update, sf::Vector3<unsigned int> position)
{
	sf::Vector2f	offset;
	sf::Rect<int>	area;
	unsigned int	count = 0;

	if (!to_update.get_divers().empty())
	{
		auto current = to_update.get_divers().begin();
		while (current != to_update.get_divers().end() && count < OBJECT_AMOUNT)
		{
			offset = (*current)->get_offset_text();
			offset += g_textures.get_offset((*current)->get_index_text());
			area = g_textures.get_area((*current)->get_index_text());

			this->_fill_quad(this->_get_quad(2 + count, position), offset, area);
			count++;
		}
	}
	for (; count < OBJECT_AMOUNT; count++)
		this->_reset_quad(this->_get_quad(2 + count, position));
}

void		Vertex_Array::update_character(Case &to_update, sf::Vector3<unsigned int> position)
{
	sf::Vector2f	offset;
	sf::Rect<int>	area;

	if (!to_update.get_occupant().empty())
	{
		auto current = to_update.get_occupant().begin();
		offset = (*current)->get_offset_text();
		offset += g_textures.get_offset((*current)->get_index_text());
		area = g_textures.get_area((*current)->get_index_text());

		this->_fill_quad(this->_get_quad(2 + OBJECT_AMOUNT, position), offset, area);
	}
	else
		this->_reset_quad(this->_get_quad(2 + OBJECT_AMOUNT, position));
}

void		Vertex_Array::display_more(void)
{
	if (this->_layers_displayed < this->_layers_amount)
	{
		this->_layers_displayed++;
		this->_vertices_displayed = this->_layers_displayed * this->_layer_size;
	}
}

void		Vertex_Array::display_less(void)
{
	if (this->_layers_displayed > 1)
	{
		this->_layers_displayed--;
		this->_vertices_displayed = this->_layers_displayed * this->_layer_size;
	}
}

unsigned int Vertex_Array::get_layers_displayed(void) const {return _layers_displayed;}

sf::Vertex	*Vertex_Array::_get_quad(short sublayer, sf::Vector3<unsigned int> position)
{
	sf::Vertex		*quad;
	unsigned int	index;

	index = this->_layer_size * position.z;
	index += this->_sub_layer_size * sublayer;
	index += (position.x + (position.y * this->_layer_width)) * 4;
	quad = &this->_vertices[index];

	return quad;
}

void		Vertex_Array::_fill_quad(sf::Vertex *quad, sf::Vector2f &offset, sf::Rect<int> &area)
{
	quad[0].position = sf::Vector2f(offset.x, offset.y);
	quad[1].position = sf::Vector2f(offset.x + area.width, offset.y);
	quad[2].position = sf::Vector2f(offset.x + area.width, offset.y + area.height);
	quad[3].position = sf::Vector2f(offset.x, offset.y + area.height);

	quad[0].texCoords = sf::Vector2f(area.left, area.top);
	quad[1].texCoords = sf::Vector2f(area.left + area.width, area.top);
	quad[2].texCoords = sf::Vector2f(area.left + area.width, area.top + area.height);
	quad[3].texCoords = sf::Vector2f(area.left, area.top + area.height);
}

void		Vertex_Array::_reset_quad(sf::Vertex *quad)
{
	quad[0].position = sf::Vector2f(0, 0);
	quad[1].position = sf::Vector2f(0, 0);
	quad[2].position = sf::Vector2f(0, 0);
	quad[3].position = sf::Vector2f(0, 0);

	quad[0].texCoords = sf::Vector2f(0, 0);
	quad[1].texCoords = sf::Vector2f(0, 0);
	quad[2].texCoords = sf::Vector2f(0, 0);
	quad[3].texCoords = sf::Vector2f(0, 0);
}

void		Vertex_Array::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	unsigned int	vertices_rest;
	unsigned int	vertices_start;

	states.transform *= getTransform();
	states.texture = &g_textures.get_texture();
	vertices_rest = this->_vertices_displayed;
	vertices_start = 0;
	while (vertices_rest > BATCH_MAX_SIZE)
	{
		target.draw(&this->_vertices[vertices_start], BATCH_MAX_SIZE, sf::Quads, states);
		vertices_rest -= BATCH_MAX_SIZE;
		vertices_start += BATCH_MAX_SIZE;
	}
	target.draw(&this->_vertices[vertices_start], vertices_rest, sf::Quads, states);
}
