/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Entite_Affichable.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/16 22:14:33 by plastic           #+#    #+#             */
/*   Updated: 2015/09/02 01:17:01 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Entite_Affichable.hpp>

Entite_Affichable::Entite_Affichable()
: _index_text(0), _position(0, 0, 0), _offset(0, 0) {}

Entite_Affichable::Entite_Affichable(int index_text)
: _index_text(index_text), _position(0, 0, 0), _offset(0, 0) {}

Entite_Affichable::Entite_Affichable(int index_text, int x, int y, int z)
: _index_text(index_text), _position(x, y, z), _offset(0, 0) {}

Entite_Affichable::~Entite_Affichable() {}

void			Entite_Affichable::set_sprite(int index, int x, int y, int z)
{
	this->_index_text = index;
	this->_position.x = x;
	this->_position.y = y;
	this->_position.z = z;
	this->_update_sprite();
}

void			Entite_Affichable::set_sprite(int index, Position const &pos)
{
	this->set_sprite(index, pos.get_x(), pos.get_y(), pos.get_z());
}

void			Entite_Affichable::set_index_text(int index_text)
{
	this->_index_text = index_text;
	this->_update_sprite();
}

int				Entite_Affichable::get_index_text(void) const
{
	return this->_index_text;
}

void			Entite_Affichable::set_sprite_position(int x, int y, int z)
{
	this->_position.x = x;
	this->_position.y = y;
	this->_position.z = z;
	this->_update_sprite();
}

sf::Vector3i	Entite_Affichable::get_sprite_position(void)
{
	return this->_position;
}

sf::Vector2f	Entite_Affichable::get_offset_text(void)
{
	return this->_offset;
}

void			Entite_Affichable::_update_sprite(void)
{

	if (this->_position.x == -1 && this->_position.y == -1 && this->_position.z == -1)
		return;
	else
	{
		this->_offset.x = OFFSET_XY_HALF * this->_position.x;
		this->_offset.x -= OFFSET_XY_HALF * this->_position.y;

		this->_offset.y = OFFSET_XY_QUART * this->_position.y;
		this->_offset.y += OFFSET_XY_QUART * this->_position.x;
		this->_offset.y -= OFFSET_Z * this->_position.z;
	}
}
