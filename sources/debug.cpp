#include "debug.hpp"

#include <string>
#include <iostream>

void	debug(std::string text)
{
	std::cout << text << std::endl;
}