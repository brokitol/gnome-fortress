/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Creature_Vivante.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/30 17:09:55 by vjacquie          #+#    #+#             */
/*   Updated: 2015/11/01 17:01:23 by vinz             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Creature_Vivante.hpp"

Creature_Vivante::Creature_Vivante( void ): vie(300), fatigue(300), soif(300), faim(300) {}

Creature_Vivante::~Creature_Vivante( void ) {}


Creature_Vivante::Creature_Vivante( Creature_Vivante const & src ) { *this = src; }

Creature_Vivante & Creature_Vivante::operator=( Creature_Vivante const & rhs )
{
	if (this != &rhs)
	{

	}
	return (*this);
}

void Creature_Vivante::set_vie(int vie) { this->vie = vie; }
void Creature_Vivante::set_fatigue(int fatigue) { this->fatigue = fatigue; }
void Creature_Vivante::set_soif(int soif) { this->soif = soif; }
void Creature_Vivante::set_faim(int faim) { this->faim = faim; }

void Creature_Vivante::dec_stats( void ) {
	this->soif = (this->soif - 1 >= 0) ? (this->soif - 1) : 0;
	this->faim = (this->faim - 1 >= 0) ? (this->faim - 1) : 0;
	
	std::cout << "faim = " << this->faim << " soif = " << this->soif << std::endl;
	if ((this->soif <= 0 || this->faim <= 0) && this->vie >= 1)
		this->vie--;
	else if ((this->soif <= 0 || this->faim <= 0) && this->vie <= 1)
		this->die();
}

void Creature_Vivante::die( void ) {}

int Creature_Vivante::get_vie( void ) const {return vie;}
int Creature_Vivante::get_fatigue( void ) const {return fatigue;}
int Creature_Vivante::get_soif( void ) const {return soif;}
int Creature_Vivante::get_faim( void ) const {return faim;}
