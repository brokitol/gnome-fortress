// by DamDam

#include "Tache.hpp"
#include "debug.hpp"
#include "Moteur.hpp"
#include "Case.hpp"

#define DEBUG_LEVEL	0

unsigned long int Tache::unique_id_current = 1;

// Constructors
Tache::Tache(void)
{
	this->unique_id = this->unique_id_current;
	this->unique_id_current++;
	PDEBUG(5, "Tache constructed !");
}

Tache::Tache(Tache const &src)
{
	*this = src;
	PDEBUG(5, "Tache constructed !");
}

Tache::Tache(std::string action, Position target, std::string recette)
: action(action), target(target), recette(recette)
{
	this->unique_id = this->unique_id_current;
	this->unique_id_current++;
	PDEBUG(5, "Tache constructed !");
}

// Destructor
Tache::~Tache(void)
{
	PDEBUG(5, "Tache destructed !");
}

// Accessors

std::string			Tache::getAction(void) const { return this->action; }
Position			Tache::getTarget(void) const { return this->target; }
std::string			Tache::getRecette(void) const { return this->recette; }
unsigned long int	Tache::getUniqueId(void) const { return this->unique_id; }

void				Tache::setAction(std::string action) { this->action = action; }
void				Tache::setTarget(Position target) { this->target = target; }
void				Tache::setRecette(std::string recette) { this->recette = recette; }
void				Tache::setUniqueId(unsigned long int unique_id) { this->unique_id = unique_id; };

void				Tache::execute(void)
{
	if (this->recette == "dig")
	{
		try
		{
			Case &down = the_moteur->get_data()->get_case(this->target.get_x(), this->target.get_y(), this->target.get_z() - 1);
			down.set_contenu(NULL);
			Case &c = the_moteur->get_data()->get_case(this->target);
			c.set_sol(NULL);
		}
		catch(std::exception &e){}

	}
	else
	{
		Recette		recette = Recette::getRecetteBaseByUniqueId(this->recette);

		PDEBUG(2, "Moteur - Tache : Execute recette '" + recette.getName() + "'");
		recette.execute(this->target);
	}
}

bool				Tache::getLocked(void) const { return this->locked; }
void				Tache::setLocked(bool const locked) { this->locked = locked; }

// Operator overloads

Tache				&Tache::operator=(Tache const &rhs)
{
	this->unique_id = rhs.getUniqueId();
	this->action = rhs.getAction();
	this->target = rhs.getTarget();
	this->recette = rhs.getRecette();

	return *this;
}


/*std::ostream				&operator<<(std::ostream &stream, Tache const &item)
{
	//stream << item.toString();
	return stream;
}*/


