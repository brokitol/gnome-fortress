/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Entite_Mouvante.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/04 17:21:49 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/18 11:41:50 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Entite_Mouvante.hpp"
#include "Pathfinding.hpp"
#include <time.h>	// time
#include <stdlib.h> // rand - srand
#include <iterator> // distance
#include "Moteur.hpp"
#include <debug.hpp>

#define DEBUG_LEVEL 3

Entite_Mouvante::Entite_Mouvante( void ) {}
Entite_Mouvante::Entite_Mouvante( Position position ): pos(position) {}
Entite_Mouvante::Entite_Mouvante( Entite_Mouvante const & src ) { *this = src; }
Entite_Mouvante::~Entite_Mouvante( void ) {}

Entite_Mouvante & Entite_Mouvante::operator=( Entite_Mouvante const & rhs )
{
	this->path	= rhs.path;
	this->pos	= rhs.pos;
	
	return *this;
}

Position	Entite_Mouvante::get_test_pos( void ) // test function
{
	int x_max = the_moteur->get_data()->get_largeur() - 1;
	int y_max = the_moteur->get_data()->get_largeur() - 1;
	return (Position(rand() % x_max, rand() % y_max, this->pos.get_z()));
}

Position	Entite_Mouvante::get_pos( void ) {return this->pos;}
void	Entite_Mouvante::move_to_pos(Position pos) {ret_path(pos);}
void	Entite_Mouvante::ret_path(Position pos) {this->path = Pathfinding::get_path(this->pos, pos, *this);}
void	Entite_Mouvante::nouveau_tour(void) {}

void	Entite_Mouvante::move( void )
{
	while (this->path.get_path_size() <= 0)
	{
		PDEBUG(2, "Entite_Mouvante : cherche un nouveau chemin");
		ret_path(get_test_pos());
	}
	Case & old_cas = the_moteur->get_case(this->pos);
	old_cas -= this->w_ptr.lock();
	Position newpos = this->path.next();
	if (not traverse(the_moteur->get_case(newpos)))
	{
		if (newpos == this->path.last())
		{
			PDEBUG(2, "Entite_Mouvante : destination invalide, abandon du chemin");
			do {
				PDEBUG(2, "Entite_Mouvante : cherche un nouveau chemin");
				ret_path(get_test_pos());
			}
			while (this->path.get_path_size() <= 0);
		}
		else
		{
			ret_path(this->path.last());
			newpos = this->path.next();
		}
	}
	
	PDEBUG(3, "Entite_Mouvante : move from x: " + std::to_string(this->pos.get_x()) + " y: " + std::to_string(this->pos.get_y()) + " z: " + std::to_string(this->pos.get_z()) + " to x: " + std::to_string(newpos.get_x()) + " y: " + std::to_string(newpos.get_y()) + " z: " + std::to_string(newpos.get_z()));
	this->pos.set_x(newpos.get_x());
	this->pos.set_y(newpos.get_y());
	this->pos.set_z(newpos.get_z());
	this->set_sprite_position(newpos.get_x(), newpos.get_y(), newpos.get_z());
	this->pos.set_angle(newpos.get_angle());
	Case & new_cas = the_moteur->get_case(this->pos);
	new_cas += this->w_ptr.lock();
	
}

bool	Entite_Mouvante::is_accessible(Position pos)
{
	(void)pos;
	return true;
}

bool	Entite_Mouvante::traverse(Case const &c) const
{
	if (c.get_sol() == NULL)
		return false;
	
	if (c.get_contenu() != NULL)
	{
		if (c.get_contenu()->getUnique_id() == "000020") // Escalier en pierre
			return true;
		return false;
	}
	return true;
}
