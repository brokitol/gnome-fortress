/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Path.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 14:10:35 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/02 16:55:49 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Path.hpp"
#include <iostream>
#include <debug.hpp>

#define DEBUG_LEVEL 0

Path::Path(std::list<Position> lst) : lst(lst)
{
	PDEBUG(3, std::to_string(lst.size()) + " " + std::to_string(this->lst.size()));
	actuel = this->lst.begin();
}

Path::Path(Path const &src)
{
	*this = src;
}

Path::~Path() {}

Position	Path::next()
{
	if (this->actuel == this->lst.end())
		return this->lst.back();
	auto tmp = *(this->actuel);
	this->actuel++;
	return tmp;
	
}

Position	Path::front() const {return this->lst.front();}
Position	Path::back() const {return this->lst.back();}
Position	Path::last() const {return back();}

int			Path::get_path_size( void )
{
	if (this->lst.size() <= 0)
		return (0);
	return (std::distance(this->actuel, this->lst.end()));
}

Path &		Path::operator=(Path const &rhs)
{
	this->lst = rhs.lst;
	this->actuel = lst.begin();
	for (auto i = this->lst.begin() ; i != this->lst.end() ; i++)
	{
		if (i == rhs.actuel)
		{
			this->actuel = i;
			break ;
		}
	}
	return *this;
}
