/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Moteur.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/30 16:25:49 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/15 17:18:10 by vinz             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Moteur.hpp"
#include "Gnome.hpp"
#include <iostream>
#include <unistd.h>
#include "debug.hpp"
#include <thread>         // std::thread
#include <stdlib.h>
#include <cmath>

#define DEBUG_LEVEL 3

Moteur::Moteur() {}
Moteur::~Moteur() {delete this->data;}

void									Moteur::set_end(bool val)	{this->end = val;}
bool									Moteur::get_end(void)		{return this->end;}
Case &									Moteur::get_case(Position const &pos) {return this->data->get_case(pos);}
Data *									Moteur::get_data() {return this->data;}
Liste_Taches *							Moteur::get_liste_tache( void ) { return &(this->lst_tache); }
std::list< std::shared_ptr< Objet > > *	Moteur::get_liste_obj( void ) { return &(this->lst_obj); }
std::shared_ptr< Objet >				Moteur::get_obj( unsigned long int id )
{
	for (auto it = lst_obj.begin() ; it != lst_obj.end() ; it++)
	{
		if ((*it)->get_obj_id() == id)
			return *it;
	}

	return std::shared_ptr<Objet>();
}

Moteur &	Moteur::operator+=(std::shared_ptr<Entite_Mouvante> obj)
{
	this->lst_entite.push_back(obj);
	return *this;
}
Moteur &	Moteur::operator-=(std::shared_ptr<Entite_Mouvante> obj)
{
	this->lst_entite.remove(obj);
	return *this;
}
Moteur &	Moteur::operator+=(std::shared_ptr<Objet> obj)
{
	this->lst_obj.push_back(obj);
	return *this;
}
Moteur &	Moteur::operator-=(std::shared_ptr<Objet> obj)
{
	this->lst_obj.remove(obj);
	return *this;
}

Data *		Moteur::nouveau_jeu_custom(int hauteur, int largeur, int longeur, int base_radius, int base_smooth, unsigned int seed)
{
	PDEBUG(3, "Moteur : debut nouveau_jeu");

	std::vector<std::vector<Case>> tmp(std::vector<std::vector<Case>>(largeur, std::vector<Case>(longeur)));
	std::vector<std::vector<std::vector<Case>>> * monde = new std::vector<std::vector<std::vector<Case>>>(hauteur, tmp);

	PDEBUG(3, "Moteur : creation data");
	this->data = new Data(monde, hauteur, largeur, longeur);

	Generator gen(*(this->data));
	PDEBUG(3, "Moteur : generation map");
	gen.generate_map(base_radius, base_smooth, seed);
	PDEBUG(3, "Moteur : generation entites mouvantes");
	gen.generate_entities(this->lst_entite);

	this->add_chaise(Position(3, 5, this->data->get_hauteur() - 5));
	this->add_chaise(Position(3, 7, this->data->get_hauteur() - 5));
	this->add_chaise(Position(3, 9, this->data->get_hauteur() - 5));

	this->add_table(Position(5, 6, this->data->get_hauteur() - 5));
	this->add_table(Position(5, 8, this->data->get_hauteur() - 5));

	this->add_pomme(Position(4, 5, this->data->get_hauteur() - 5));
	this->add_pomme(Position(4, 7, this->data->get_hauteur() - 5));
	this->add_pomme(Position(4, 9, this->data->get_hauteur() - 5));

	this->add_gourde(Position(7, 6, this->data->get_hauteur() - 5));
	this->add_gourde(Position(7, 8, this->data->get_hauteur() - 5));

	PDEBUG(3, "Moteur : fin nouveau_jeu");
	return this->data;
}

void	Moteur::run(void)
{
	boucle = new std::thread(&Moteur::boucle_de_jeu, this);

	boucle->detach();

	PDEBUG(3, "Moteur : boucle lancé");
}

void	Moteur::boucle_de_jeu()
{
	PDEBUG(3, "debut boucle");
	while (this->end == false)
	{
		PDEBUG(3, "Moteur : nb objet : " + std::to_string(lst_obj.size()));
		if (this->data->get_pause() == true)
		{
			PDEBUG(2, "Moteur : sleep en pause de 1s");
			sleep(1);
			continue;
		}
		std::shared_ptr<Creature_Vivante> morts;
		for (std::shared_ptr<Entite_Mouvante> ent : this->lst_entite)
		{
			PDEBUG(3, "Moteur : appel de nouveau tour");
			std::shared_ptr<Creature_Vivante> c_tmp = std::dynamic_pointer_cast<Creature_Vivante >(ent);
			if (c_tmp and c_tmp->get_vie() <= 0)
				morts = c_tmp;
			else
				ent->nouveau_tour();
		}
		if (morts)
		{
			*this -= morts;
			this->get_case(morts->get_pos()) -= morts;
		}
		PDEBUG(2, "Moteur : sleep1 en run de 0.2s");
		usleep(200000);
	}
	PDEBUG(3, "fin boucle");
}


void	Moteur::add_tache_chaise(Position const &p) {this->add_chaise(p);}
void	Moteur::add_tache_chaise() {this->add_tache_chaise(Position(6, 6, this->data->get_hauteur() - 5));}
void	Moteur::add_chaise(Position const &pos) {this->lst_tache.addTache(Tache("CreerChaise", pos, "000001"));}
void	Moteur::add_table(Position const &pos) {this->lst_tache.addTache(Tache("CreerTable", pos, "000011"));}
void	Moteur::add_pomme(Position const &pos) {this->lst_tache.addTache(Tache("CreerPomme", pos, "000030"));}
void	Moteur::add_gourde(Position const &pos) {this->lst_tache.addTache(Tache("CreerGourde", pos, "000020"));}
void	Moteur::add_tache(std::string nom_action, Position pos, std::string num_recette) {this->lst_tache.addTache(Tache(nom_action, pos, num_recette));}
