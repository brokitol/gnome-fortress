#include "Recette.hpp"
#include "Case.hpp"
#include "Moteur.hpp"
#include "Objet.hpp"
#include <memory>
#include <list>
#include "debug.hpp"
#include <iostream>
#include <fstream>
#include <sstream>

#define DEBUG_LEVEL 0

std::list<Recette>		Recette::recettes_base;

// Constructors
Recette::Recette(void)
{
	PDEBUG(5, "Recette void-constructed !");
}

Recette::Recette(Recette const &src)
{
	*this = src;
	PDEBUG(5, "Recette copy-constructed ! - Uid : " + this->unique_id + " - Name : " + this->name + " - TypeAction : " + std::to_string(this->type_action) + " - ReqItems : <req_items> - ResultItem : " + this->result_item);
}

Recette::Recette(std::string unique_id, std::string name, int type_action, std::list<std::string> req_items_list, std::string result_item)
: unique_id(unique_id), name(name), type_action(type_action), req_items_list(req_items_list), result_item(result_item)
{
	PDEBUG(5, "Recette full-constructed ! - Uid : " + unique_id + " - Name : " + name + " - TypeAction : " + std::to_string(type_action) + " - ReqItems : <req_items> - ResultItem : " + result_item);
}

// Destructor
Recette::~Recette(void)
{
	PDEBUG(6, "Recette destructed !");
}

// Accessors

std::string				Recette::getUnique_id(void) const { return this->unique_id; }
std::string				Recette::getName(void) const { return this->name; }
int						Recette::getType_action(void) const { return this->type_action; }
std::list<std::string>	Recette::getReq_items_list(void) const { return this->req_items_list; }
std::string				Recette::getResult_item(void) const { return this->result_item; }

void					Recette::setUnique_id(std::string unique_id) { this->unique_id = unique_id; }
void					Recette::setName(std::string name) { this->name = name; }
void					Recette::setType_action(int type_action) { this->type_action = type_action; }
void					Recette::setReq_items_list(std::list<std::string> req_items_list) { this->req_items_list = req_items_list; }
void					Recette::setResult_item(std::string result_item) { this->result_item = result_item; }

bool					Recette::prepare(Position &target)
{
	//static_cast<void>(target);
	Case &the_case = the_moteur->get_case(target);
	for (auto req_item : this->getReq_items_list())
	{
		for (auto obj : the_case.get_divers())
		{
			obj->setLocked(true);
		}
	}
	
	return true;
}

bool			Recette::execute(Position &target)
{
	PDEBUG(4, "Recette '" + this->name + "' execution...");
	Objet::copy_and_spawn(Objet::getObjetBaseByUniqueId(this->result_item), target);
	PDEBUG(4, "Recette '" + this->name + "' executed !");
	return true;
}

bool			Recette::execute_and_spawn(Position &target, Objet &obj)
{
	Objet::copy_and_spawn(obj, target);

	return true;
}


void					Recette::chargerRecettesBase(void)
{
	std::ifstream		file;
	std::string			line;

	std::string			unique_id;
	std::string			name;
	std::string			type_action;
	std::string			req_items_list;
	std::string			result_item;

	file.open("Data/recettes.dat");
	if (!file.good())
		throw std::exception();
	PDEBUG(3, "File open ok !");

	Recette::recettes_base.clear();

	while (getline(file, line))
	{
		std::stringstream	parse_line(line);

		if (!getline(parse_line, unique_id, ':'))
			throw std::exception();
		if (!getline(parse_line, name, ':'))
			throw std::exception();
		if (!getline(parse_line, type_action, ':'))
			throw std::exception();
		if (!getline(parse_line, req_items_list, ':'))
			throw std::exception();
		if (!getline(parse_line, result_item))
			throw std::exception();

		PDEBUG(2, "Uid : " + unique_id + " - Name : " + name + " - TypeAction : " + type_action + " - ReqItems : " + req_items_list + " - ResultItem : " + result_item + " (" + line + ")");
		
		Recette::recettes_base.push_back(Recette(unique_id, name, stoi(type_action), std::list<std::string>(3, "000001"), result_item));
	}

	#if DEBUG_LEVEL >= 1
		for (auto rec : Recette::recettes_base)
		{
			debug(rec.getUnique_id() + "|" + rec.getName() + "|" + std::to_string(rec.getType_action()) + "| <req_items> |" + rec.getResult_item());
		}
	#endif

	PDEBUG(3, "File read ok !");
}

Recette				Recette::getRecetteBaseByUniqueId(std::string unique_id)
{
	if (Recette::recettes_base.empty())
		Recette::chargerRecettesBase();

	if (Recette::recettes_base.empty())
		throw std::exception();

	for (auto rec : Recette::recettes_base)
	{
		if (rec.getUnique_id() == unique_id)
			return Recette(rec.getUnique_id(), rec.getName(), rec.getType_action(), rec.getReq_items_list(), rec.getResult_item());
	}

	return Recette("ERROR", "ERROR_INIT", 0, std::list<std::string>(), "ERROR");
}



// Operator overloads

Recette				&Recette::operator=(Recette const &rhs)
{
	this->unique_id = rhs.getUnique_id();
	this->name = rhs.getName();
	this->type_action = rhs.getType_action();
	this->req_items_list = rhs.getReq_items_list();
	this->result_item = rhs.getResult_item();

	return *this;
}

/*
std::ostream				&operator<<(std::ostream &stream, Recette const &item)
{
	//stream << item.toString();
	return stream;
}
*/

