/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pathfinding.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 14:10:38 by bgauci            #+#    #+#             */
/*   Updated: 2015/11/02 17:14:52 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pathfinding.hpp"
#include "debug.hpp"
#include "Node.hpp"
#include "Data.hpp"
#include "Moteur.hpp"
#include <cstdlib>

#define DEBUG_LEVEL 0

//Path	Pathfinding::get_path_a_star(Position const depart, Position const arriver, Entite_Mouvante &entite)
Path	Pathfinding::get_path(const Position depart, const Position arriver, Entite_Mouvante &entite)
{
	PDEBUG(2, "demande chemin de x: " + std::to_string(depart.get_x()) + " y: " + std::to_string(depart.get_y()) + " z: " + std::to_string(depart.get_z()) + " vers x : " + std::to_string(arriver.get_x()) + " y: " + std::to_string(arriver.get_y()) + " z: " + std::to_string(arriver.get_z()));
	
	if (not the_moteur->get_data()->get_case(arriver).est_traversable(entite))
	{
		PDEBUG(2, "destination invalide, pas de chemin possible");
		return Path();
	}

	return get_path_a_star(depart, arriver, entite);
}

static bool node_fait(std::list<Node*> list_fait, Node * n1)
{
	PDEBUG(3, "node fait : debut");
	for ( Node * n2 : list_fait)
		if (*n2 == *n1) {return true;}
	PDEBUG(3, "node fait : false");
	return false;
}

// predicate pour le remove_if
static Node *	predi_var;
// predicate pour le remove_if
static bool	predicate(const Node *val)
{
	return (*predi_var == *val) ? true : false;
}


//*
Path	Pathfinding::get_path_a_star(Position const depart, Position const arriver, Entite_Mouvante &entite)
{
	Data *data = the_moteur->get_data();

	//init
	std::list<Node*>	list_actu;
	std::list<Node*>	list_fait;
	Node	*actu = NULL;

	list_actu.push_front(new Node(depart, arriver));

	while (list_actu.empty() == false)
	{
		PDEBUG(3, "debut de boucle: " + std::to_string(list_actu.size()));

		actu = NULL;
		for (Node *n : list_actu)//parcourir la liste de node
		{
			if (actu == NULL || *actu > *n)//garder la plus proche du point d'arriver
				actu = n;
		}
		
		if (*actu == arriver)
			break;

		predi_var = actu;	
		list_actu.remove_if(predicate);
		list_fait.push_back(actu);
		Position	p = actu->get_position();
		if (p.get_x() < data->get_largeur() - 1)
		{
			PDEBUG(3, "test x+");
			Position p_tmp = p;
			p_tmp.set_x(p_tmp.get_x() + 1);
			Node * n = new Node(p_tmp, arriver, actu);
			PDEBUG(3, "	variable set : " + std::to_string(p_tmp.get_x()));
			if (not node_fait(list_fait, n) && data->get_case(p_tmp).est_traversable(entite))
				list_actu.push_front(n);
		}
		if (p.get_x() > 0)
		{
			PDEBUG(3, "test x-");
			Position p_tmp = p;
			p_tmp.set_x(p_tmp.get_x() - 1);
			Node * n = new Node(p_tmp, arriver, actu);
			PDEBUG(3, "	variable set");
			if (not node_fait(list_fait, n) && data->get_case(p_tmp).est_traversable(entite))
				list_actu.push_front(n);
		}
		if (p.get_y() < data->get_longeur() - 1)
		{
			PDEBUG(3, "test y+");
			Position p_tmp = p;
			p_tmp.set_y(p_tmp.get_y() + 1);
			Node * n = new Node(p_tmp, arriver, actu);
			PDEBUG(3, "	variable set");
			if (not node_fait(list_fait, n) && data->get_case(p_tmp).est_traversable(entite))
				list_actu.push_front(n);
		}
		if (p.get_y() > 0)
		{
			PDEBUG(3, "test y-");
			Position p_tmp = p;
			p_tmp.set_y(p_tmp.get_y() - 1);
			Node * n = new Node(p_tmp, arriver, actu);
			PDEBUG(3, "	variable set");
			if (not node_fait(list_fait, n) && data->get_case(p_tmp).est_traversable(entite))
				list_actu.push_front(n);
		}
		if (p.get_z() < data->get_hauteur() - 1)
		{
			PDEBUG(3, "test z+");
			Position p_tmp = p;
			p_tmp.set_z(p_tmp.get_z() + 1);
			Node *	n = new Node(p_tmp, arriver, actu);
			PDEBUG(3, "	variable set");
			if (not node_fait(list_fait, n) && data->get_case(p_tmp).est_traversable(entite))
				list_actu.push_front(n);
		}
		if (p.get_z() > 0)
		{
			PDEBUG(3, "test z-");
			Position p_tmp = p;
			p_tmp.set_z(p_tmp.get_z() - 1);
			Node * n = new Node(p_tmp, arriver, actu);
			PDEBUG(3, "	variable set");
			if (not node_fait(list_fait, n) && data->get_case(p_tmp).est_traversable(entite))
				list_actu.push_front(n);
		}
	}
	PDEBUG(3, "fin de recherche");

	if (*actu != arriver) // aucun chemin existe
	{
		PDEBUG(2, "pas de chemin");
		return Path();
	}
	std::list<Position>	lst;

	PDEBUG(3, "recreation du chemin");
	while (actu && actu->get_precedente() != NULL)
	{
		if (actu == actu->get_precedente())
		{	
			PDEBUG(2, "boucle infini !!!");
		}
			
		lst.push_front(actu->get_position());
		actu = actu->get_precedente();
	}

	PDEBUG(2, "taille du chemin : " + std::to_string(lst.size()));
	return Path(lst);
}//*/
