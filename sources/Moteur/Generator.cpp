
#include "Generator.hpp"
#include "Gnome.hpp"
#include "Objet.hpp"
#include <debug.hpp>
#include <math.h>
#include <algorithm>
#include <array>

#define DEBUG_LEVEL 3


// Constructors

Generator::Generator(Generator const &src) : data(src.get_data())
{
	*this = src;
	PDEBUG(5, "Generator constructed !");
}

Generator::Generator(Data &data) : data(data)
{
	this->data = data;
	PDEBUG(5, "Generator constructed !");
}

// Destructor

Generator::~Generator(void) {PDEBUG(5, "Generator destructed !");}

// Accessors

Data			&Generator::get_data(void) const { return this->data; }

void			Generator::set_data(Data &data) { this->data = data; }

// Operator overloads

Generator				&Generator::operator=(Generator const &rhs)
{
	this->data = rhs.get_data();
	
	return *this;
}

// Custom methods

bool		Generator::map_add_hole(Position const &pos, int radius, int depth)
{
	int	largeur = this->data.get_largeur(); // x
	int	longeur = this->data.get_longeur(); // y
	int	hauteur = this->data.get_hauteur(); // z

	for (int z = pos.get_z(); z >= (int)pos.get_z() - (depth - 1); z--)
	{
		if (z < 0 || z > hauteur - 1)
			continue;
		
		for (int x = pos.get_x() - radius + 1; x <= (int)pos.get_x() + radius - 1; x++)
		{
			if (x < 0 || x > largeur - 1)
				continue;
			
			for (int y = pos.get_y() - radius + 1; y <= (int)pos.get_y() + radius - 1; y++)
			{
				if (y < 0 || y > longeur - 1)
					continue;

				Case &c = this->data.get_case(Position(x, y, z));
				c.set_sol(NULL);
				c.set_contenu(NULL);
			}
		}
	}

	return true;
}

bool		Generator::map_add_height(Position const &pos, int radius, int height)
{
	int	largeur = this->data.get_largeur(); // x
	int	longeur = this->data.get_longeur(); // y
	int	hauteur = this->data.get_hauteur(); // z

	for (int z = pos.get_z(); z <= (int)pos.get_z() + (height - 1); z++)
	{
		if (z < 0 || z > hauteur - 1)
			continue;
		
		for (int x = pos.get_x() - radius + 1; x <= (int)pos.get_x() + radius - 1; x++)
		{
			if (x < 0 || x > largeur - 1)
				continue;
			
			for (int y = pos.get_y() - radius + 1; y <= (int)pos.get_y() + radius - 1; y++)
			{
				if (y < 0 || y > longeur - 1)
					continue;

				Case &c = this->data.get_case(Position(x, y, z));
				auto sol = std::make_shared<Objet>("000001");
				sol->set_sprite(1, x, y, z);
				c.set_sol(sol);
				auto contenu = std::make_shared<Objet>("000002");
				contenu->set_sprite(3, x, y, z);
				c.set_contenu(contenu);
			}
		}
	}

	return true;
}

bool		Generator::map_add_rmountain(Position const &pos, int radius, int depth, int smooth, int cut)
{
	int	largeur = this->data.get_largeur(); // x
	int	longeur = this->data.get_longeur(); // y
	int	hauteur = this->data.get_hauteur(); // z

	for (int z = pos.get_z(); z >= (int)pos.get_z() - (depth - 1 - cut); z--)
	{
		if (z < 0 || z > hauteur - 1)
			continue;
		
		for (int x = pos.get_x() - radius + 1; x <= (int)pos.get_x() + radius - 1; x++)
		{
			if (x < 0 || x > largeur - 1)
				continue;
			
			for (int y = pos.get_y() - radius + 1; y <= (int)pos.get_y() + radius - 1; y++)
			{
				if (y < 0 || y > longeur - 1)
					continue;

				if (( z - (pos.get_z() + 1) >= -depth + pow(std::abs(x - pos.get_x()), 2) / smooth )
					&&
					(z - (pos.get_z() + 1) >= -depth + pow(std::abs(y - pos.get_y()), 2) / smooth ))
				{
					Case &c = this->data.get_case(Position(x, y, z));
					c.set_sol(NULL);
					c.set_contenu(NULL);
				}
			}
		}
	}

	return true;
}

bool		Generator::map_add_mountain(Position const &pos, int radius, int height, int smooth, int cut)
{
	int	largeur = this->data.get_largeur(); // x
	int	longeur = this->data.get_longeur(); // y
	int	hauteur = this->data.get_hauteur(); // z

	for (int z = pos.get_z(); z <= (int)pos.get_z() + (height - 1 - cut); z++)
	{
		if (z < 0 || z > hauteur - 1)
			continue;
		
		for (int x = pos.get_x() - radius + 1; x <= (int)pos.get_x() + radius - 1; x++)
		{
			if (x < 0 || x > largeur - 1)
				continue;
			
			for (int y = pos.get_y() - radius + 1; y <= (int)pos.get_y() + radius - 1; y++)
			{
				if (y < 0 || y > longeur - 1)
					continue;

				if (( z - pos.get_z() + 1 <= height + -1 * pow(std::abs(x - pos.get_x()), 2) / smooth )
					&&
					(z - pos.get_z() + 1 <= height + -1 * pow(std::abs(y - pos.get_y()), 2) / smooth ))
				{
					Case &c = this->data.get_case(Position(x, y, z));
					auto sol = std::make_shared<Objet>("000001");
					sol->set_sprite(1, x, y, z);
					c.set_sol(sol);
					auto contenu = std::make_shared<Objet>("000002");
					contenu->set_sprite(3, x, y, z);
					c.set_contenu(contenu);
				}
			}
		}
	}

	return true;
}

void		Generator::generate_map(int base_radius, int base_smooth, unsigned int seed)
{
	/* Map format :
			z
			|
		   / \
		  y   x

	   Data format :
	     z, x, y

	   Position format :
	     x, y, z
	*/

	int		max_x = this->data.get_largeur();
	int		max_y = this->data.get_longeur();
	int		max_z = this->data.get_hauteur();

	PDEBUG(3, "Moteur : test taille");
	if (max_z < 11 or max_x < 11 or max_y < 11) {PDEBUG(3, "Moteur : erreur taille map");exit(0);} // taille minimum

	
	for (int h = 0 ; h < max_z - 5 ; h++)
	{
		for (int la = 0 ; la < max_x ; la++)
		{
			for (int lo = 0 ; lo < max_y ; lo++)
			{
				Case &c = this->data.get_case(Position(la,lo,h)); // sous-sol
				PDEBUG(4, "Moteur : creation de sol en " + std::to_string(la) + "," + std::to_string(lo) + "," + std::to_string(h));
				auto sol = std::make_shared<Objet>("000001");
				sol->set_sprite(1, la, lo, h);
				c.set_sol(sol);
				auto contenu = std::make_shared<Objet>("000002");
				contenu->set_sprite(3, la, lo, h);
				c.set_contenu(contenu);
			}
		}
	}

	PDEBUG(3, "Moteur : sous-sol fait");

	// Generate random map "level"

	if (seed > 0)
		std::srand(seed);

	int rand_x = std::rand() % max_x;
	int rand_y = std::rand() % max_y;
	int rand_z = std::rand() % max_z;

	int rand_depth = std::rand() % max_z;
	int rand_height = std::rand() % max_z;

	int	rand_radius = std::rand() % base_radius;
	int	rand_smooth = std::rand() % base_smooth;

	// Generate holes
	int	x = 0, y = 0, z = 0, radius = 0, depth = 0, height = 0;

	x = max_x * 2 / 3 + rand_x * 1 / 3 + 2;
	y = max_y * 2 / 3 + rand_y * 1 / 3;
	z = max_z - 4 ;
	radius = 3 + rand_radius;
	depth = max_z * 2 / 3 + rand_depth * 2 / 3;

	this->map_add_hole(Position(x, y, z), radius, depth);

	x = max_x * 3 / 4 + rand_x / 4;
	y = rand_y * 1 / 4;
	z = max_z - 4 + rand_z / 10;
	radius = 1 + rand_radius * 3 / 5;
	depth = max_z * 2 / 3 + rand_depth / 2;

	this->map_add_hole(Position(x, y, z), radius, depth);

	x = rand_x / 3;
	y = max_y * 3 / 4 - 4 + rand_y * 1 / 4;
	z = max_z - 4;
	radius = 3;
	depth = rand_depth / 4;

	this->map_add_hole(Position(x, y, z), radius, depth);

	x = max_y - rand_x / 8;
	y = 0;
	z = (max_z - 5) / 2;
	radius = 6;
	depth = 3;

	this->map_add_hole(Position(x, y, z), radius, depth);

	x = rand_x / 2;
	y = max_y - rand_y / 8;
	z = 2;
	radius = 3 + rand_radius * 3 / 4;
	depth = 3;

	this->map_add_hole(Position(x, y, z), radius, depth);

	// Generate Heights
	x = 4;
	y = max_y - 4;
	z = max_z - 5;
	radius = 2;
	height = 1 + rand_height / 10;

	this->map_add_height(Position(x, y, z), radius, height);

	x = 0;
	y = 0;
	z = max_z - 5;
	radius = 1;
	height = 1 + rand_height / 10;

	this->map_add_height(Position(x, y, z), radius, height);


	// Test de cuvettes avec x²
	
	int smooth = 1, cut = 0;

	x = max_x / 2;
	y = max_y / 2;
	z = max_z - 6;
	radius = std::max(max_x / 2, max_y / 2);
	depth = max_z / 5 + rand_depth / 4;;
	smooth = 6 + rand_smooth;
	cut = 0;

	this->map_add_rmountain(Position(x, y, z), radius, depth, smooth, cut);


	x = max_x / 2;
	y = max_y - rand_y / 9;
	z = max_z - 5;

	radius =  3 + rand_radius * 3 / 4;
	depth = 5;
	smooth = 20 + rand_smooth;
	cut = 0;

	this->map_add_rmountain(Position(x, y, z), radius, depth, smooth, cut);

	// Test de montagnes avec x²

	x = 0;
	y = max_y * 1 / 4 + rand_y * 1 / 2;
	z = max_z - 5;
	radius = std::max(max_x / 2, max_y / 2);
	height = 3;
	smooth = 3;
	cut = 0;

	this->map_add_mountain(Position(x, y, z), radius, height, smooth, cut);

	x = max_x / 2 + rand_x * 1 / 6;
	y = 3;
	z = max_z - 6;
	radius = 4;
	height = 3;
	smooth = 1;
	cut = 0;

	this->map_add_mountain(Position(x, y, z), radius, height, smooth, cut);

	// Spawn de l'herbe en surface

	for (int z = 1 ; z < max_z - 1 ; z++)
	{
		for (int x = 0 ; x < max_x ; x++)
		{
			for (int y = 0 ; y < max_y ; y++)
			{
				Case &c = this->data.get_case(x, y, z);
				if (!c.get_contenu())
				{
					Case &c2 = this->data.get_case(x, y, z - 1);
					if (c2.get_contenu())
					{
						// Herbe
						auto sol = std::make_shared<Objet>("000001");
						sol->set_sprite(2, x, y, z);
						c.set_sol(sol);

						if (rand() % 300 == 0)
						{
							// Diamant (utilisable)
							Objet::copy_and_spawn(Objet::getObjetBaseByUniqueId("001000"), Position(x, y, z));
						}
						else if (rand() % 70 == 0)
						{
							// "Pomme" (qui ressemble à une fraise) (utilisable)
							Objet::copy_and_spawn(Objet::getObjetBaseByUniqueId("000400"), Position(x, y, z));
						}
						else if (rand() % 150 == 0)
						{
							// "Gourde" (faut vraiment choisir de meilleurs noms) (utilisable)
							Objet::copy_and_spawn(Objet::getObjetBaseByUniqueId("000300"), Position(x, y, z));
						}
						else if (rand() % 20 == 0)
						{
							// "Arbre" (sapin) (élément de décor non-utilisable)
							auto obj = std::make_shared<Objet>("000005");
							obj->set_sprite(obj->get_index_text(), x, y, z);
							c += obj;
						}
					}
				}
			}
		}
	}

	for (int z = 1 ; z < max_z - 1 ; z++)
	{
		for (int x = 0 ; x < max_x ; x++)
		{
			for (int y = 0 ; y < max_y ; y++)
			{
				Case &c = this->data.get_case(x, y, z);
				if (c.get_sol() && c.get_sol()->getUnique_id() != "000003" && !c.get_contenu())
				{
					try
					{
						Case &up_right = this->data.get_case(x, y - 1, z + 1);
						if (up_right.get_sol() && up_right.get_sol()->getUnique_id() != "000003" && !up_right.get_contenu())
						{
							// S'il n'y a pas déjà du contenu ou un objet sur la case
							if (!c.get_contenu() && c.get_divers().empty())
							{
								// On met un excalier en pierre
								auto contenu = std::make_shared<Objet>("000020");
								contenu->set_sprite(contenu->get_index_text(), x, y, z);
								c.set_contenu(contenu);

								try
								{
									// On met un sol sur la case du dessus
									Case &up = this->data.get_case(x, y, z + 1);
									auto sol = std::make_shared<Objet>("000003");
									//sol->set_sprite(sol->get_index_text(), x, y, z + 1);
									up.set_sol(sol);
								}
								catch(std::exception &e){}
							}
						}
					}
					catch(std::exception &e){}	
				}
			}
		}
	}	

	

}

void	Generator::generate_entities(std::list< std::shared_ptr< Entite_Mouvante > > &entities)
{
	/*
	 * fin de la creation temporaire
	 */
	bool	gnome_spawned = false;

	std::array<std::string, 10>	names = {{"Albert", "Bobby", "Alfred", "Ren", "Robert", "Bernard", "Paul", "Edmund", "Isidore", "Armando"}};
	std::array<int, 7>			sprite_nums = {{ 9, 10, 17, 18, 19, 20, 21}};

	std::random_shuffle(std::begin(names), std::end(names));
	std::random_shuffle(std::begin(sprite_nums), std::end(sprite_nums));

	int rand_x = 0, rand_y = 0;

	int	max_x = this->data.get_largeur();
	int	max_y = this->data.get_longeur();

	for (int i = 1; i <= 6; i++)
	{
		rand_x = (std::rand() / i) % max_x;
		rand_y = (std::rand() / i) % max_y;
		
		gnome_spawned = false;
		for (int ha = this->data.get_hauteur() - 1 ; ha >= 0 ; ha--)
		{
			PDEBUG(3, "Moteur : tentative de creation de gnome en [" + std::to_string(rand_x)
			 + "," + std::to_string(rand_y) + "," + std::to_string(ha) + "]");
			Case &c = this->data.get_case(Position(rand_x, rand_y, ha));

			if (!c.get_contenu() && c.get_sol())
			{
				PDEBUG(3, "Moteur : creation de gnome");
				//auto g = std::make_shared<Gnome>(names[static_cast<int>(rand() % 5)]);
				auto g = std::make_shared<Gnome>(names[(i - 1) % names.size()]);
				PDEBUG(3, "Moteur : init du gnome '" + g->get_name() + "'");
				g->init(Position(rand_x, rand_y, ha, 0), g);
				PDEBUG(3, "Moteur : sauvegarde du gnome");
				g->set_sprite(sprite_nums[(i - 1) % sprite_nums.size()], rand_x, rand_y, ha);
				c += g;
				entities.push_back(g);
				PDEBUG(3, "Moteur : fin de la creation du gnome");

				gnome_spawned = true;

				break;
			}
		}

		if (!gnome_spawned)
			i--;
	}
	//exit(0);
}
