/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Gnome.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vinz <vinz@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/07 18:48:24 by vinz              #+#    #+#             */
/*   Updated: 2015/11/01 17:16:47 by vinz             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Gnome.hpp"
#include "Moteur.hpp"
#include "debug.hpp"

#define DEBUG_LEVEL 3

Gnome::Gnome( void ): is_task(false), search_item(0), name("<unnamed>") {PDEBUG(3, "gnome construct");}
Gnome::Gnome( std::string name ): is_task(false), search_item(0), name(name) {PDEBUG(3, "gnome construct");}
Gnome::~Gnome( void ) {}
Gnome::Gnome( Gnome const & src ) { *this = src; }

std::string		Gnome::get_name(void) const { return this->name; }
void			Gnome::set_name(std::string const name) { this->name = name; }

Tache			Gnome::get_tache(void) const { return this->task; }
bool			Gnome::get_is_task(void) const { return this->is_task; }
int				Gnome::get_search_item(void) const { return this->search_item; }
Position		Gnome::get_move_to(void) const { return this->move_to; }

Gnome & Gnome::operator=( Gnome const & rhs )
{
	this->move_to = rhs.move_to;
	this->search_item = rhs.search_item;

	return *this;
}

void	Gnome::exec_tache( void )
{
	unsigned long int		UniqueId = 0;

	if (this->is_task == false)
	{
		if (the_moteur->get_liste_tache()->isFreeTache() == false)
		return ;

		this->task = the_moteur->get_liste_tache()->getFreeTache();
		move_to_pos(this->task.getTarget());
		//this->task.setLocked(true);
		the_moteur->get_liste_tache()->lockTache(this->task.getUniqueId());
		this->is_task = true;
	}

	if (this->task.getTarget() == this->pos)
	{
		this->task.execute();
		UniqueId = this->task.getUniqueId();
		this->is_task = false;
		PDEBUG(3, "Gnome : tache fait");
		the_moteur->get_liste_tache()->removeTache(UniqueId);
	}
}

bool	Gnome::go_to_objet(std::string obj, int type)
{
	PDEBUG(3, "Gnome test2");
	//sleep(1);
	for (auto it : *(the_moteur->get_liste_obj()))
	{
		PDEBUG(3, "Gnome test : [" + it->getNom() + "]/[" + obj + "]");
		if (obj.compare(it->getNom()) == 0 && it->getLocked() == false)
		{
			this->search_item = type;
			this->move_to = it->getPosition();
			it->setLocked(true);
			this->search_item_id = it->get_obj_id();
			PDEBUG(3, "Gnome Go Go Go !!");
			move_to_pos(this->move_to);
			return (true);
		}
	}
	//sleep(2);
	return (false);
}

void	Gnome::conso_search_objet( void )
{
	std::shared_ptr<Objet> cible;

	cible = the_moteur->get_obj(this->search_item_id);
	if (cible)
	{
		*the_moteur -= cible;
		the_moteur->get_case(pos) -= cible;
		PDEBUG(3, "Objet  : " + cible->getNom() + " consome");
		this->search_item_id = 0;
	}
}

void	Gnome::nouveau_tour(void)
{
	PDEBUG(3, "gnome nouveau_tour");
	dec_stats();
	if (this->search_item == 0)
	{
		if (get_soif() < 70)
			go_to_objet("Gourde d'eau", 1);
		else if (get_faim() < 70)
			go_to_objet("Pomme", 2);
		else
			exec_tache();
	}
	else if (this->search_item != 0 && this->move_to == this->pos) // ici on doit delete l'objet qui a été consomé !
	{
		if (this->search_item == 1)
			set_soif(300);
		else if (this->search_item == 2)
			set_faim(300);
		conso_search_objet();
		this->search_item = 0;
		if (this->is_task == true)
			move_to_pos(this->task.getTarget());
	}
	move();
}

void	Gnome::init( Position position, std::weak_ptr<Gnome> this_ptr)
{
		PDEBUG(3, "Gnome : debut init");
	this->pos = position;
		PDEBUG(3, "Gnome : debut get case");
	Case & cas = the_moteur->get_case(position);
		PDEBUG(3, "Gnome : fin get case");
	cas += this_ptr.lock();
	this->w_ptr = this_ptr;
	//(void )cas;
		PDEBUG(3, "Gnome : init with pos");
	srand(time(NULL));
		PDEBUG(3, "Gnome : fin init");
}
