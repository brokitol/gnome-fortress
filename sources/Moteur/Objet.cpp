
#include "Objet.hpp"
#include "Moteur.hpp"
#include "Position.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <list>
#include <debug.hpp>

#define DEBUG_LEVEL 0

std::list<Objet>		Objet::objets_base;
unsigned long int		Objet::autoincrement = 0;
// Constructors
Objet::Objet(void) {
	PDEBUG(5, "Objet constructed !");
	Objet::autoincrement++;
	this->id = Objet::autoincrement;
}

Objet::Objet(Objet const &src)
{
	*this = src;
	PDEBUG(5, "Objet constructed !");
}

Objet::Objet(std::string unique_id) {
	*this = getObjetBaseByUniqueId(unique_id);
	Objet::autoincrement++;
	this->id = Objet::autoincrement;
}

Objet::Objet(std::string unique_id, std::string nom, int valeur_monetaire)
: unique_id(unique_id), nom(nom), valeur_monetaire(valeur_monetaire), locked(false)
{
	PDEBUG(5, "Objet constructed !");
	Objet::autoincrement++;
	this->id = Objet::autoincrement;
}

Objet::Objet(std::string unique_id, std::string nom, int image_id,  int valeur_monetaire)
: Entite_Affichable(image_id), unique_id(unique_id), nom(nom), valeur_monetaire(valeur_monetaire), locked(false)
{
	PDEBUG(5, "Objet constructed !");
	Objet::autoincrement++;
	this->id = Objet::autoincrement;
}

// Destructor
Objet::~Objet(void) {PDEBUG(5, "Objet destructed !");}

// Accessors
unsigned long int	Objet::get_obj_id( void ) const { return this->id; }
std::string			Objet::getUnique_id(void) const { return this->unique_id; }
std::string			Objet::getNom(void) const { return this->nom; }
int					Objet::getValeur_monetaire(void) const { return this->valeur_monetaire; }
bool				Objet::getLocked(void) const { return this->locked; }
Position			Objet::getPosition(void) const { return this->position; }


void				Objet::setUnique_id(std::string const unique_id) { this->unique_id = unique_id; }
void				Objet::setNom(std::string const nom) { this->nom = nom; }
void				Objet::setValeur_monetaire(int const valeur_monetaire) { this->valeur_monetaire = valeur_monetaire; }
void				Objet::setLocked(bool const locked) { this->locked = locked; }
void				Objet::setPosition(Position const position) { this->position = position; }

void				Objet::parseFile(std::string filename) throw(std::exception)
{
	std::ifstream		file;
	std::string			line;

	std::string			unique_id;
	std::string			nom;
	std::string			valeur_monetaire;

	file.open(filename);
	if (!file.good())
		throw std::exception();

	while (getline(file, line))
	{
		PDEBUG(2, line);
	}

	PDEBUG(2, "File read ok !");
}

void				Objet::spawn(std::shared_ptr<Objet> obj_ptr, Position const &target)
{
	Case &the_case = the_moteur->get_case(target);
	obj_ptr->set_sprite(obj_ptr->get_index_text(), target);
	the_case += obj_ptr;
	obj_ptr->setPosition(target);
	the_moteur->get_liste_obj()->push_back(obj_ptr);

	PDEBUG(2, "Generated new object : " + obj_ptr->getNom() + "(" + obj_ptr->getUnique_id() + ") in (" + std::to_string(target.get_x()) + ", " + std::to_string(target.get_y()) + ", " + std::to_string(target.get_z()) + ")");
}

void				Objet::copy_and_spawn(Objet const &obj, Position const &target)
{
	//Objet	new_obj = new Objet(obj);

	std::shared_ptr<Objet> new_obj = std::make_shared<Objet>(obj);

	Objet::spawn(new_obj, target);

	//PDEBUG(2, "Generated new object : " + new_obj->getNom() + "(" + new_obj->getUnique_id() + ")");

}

void				Objet::chargerObjetsBase(void)
{
	std::ifstream		file;
	std::string			line;

	std::string			unique_id;
	std::string			image_id;
	std::string			nom;
	std::string			valeur_monetaire;

	file.open("Data/objets.dat");
	if (!file.good())
		throw std::exception();
	PDEBUG(3, "File open ok !");

	Objet::objets_base.clear();

	while (getline(file, line))
	{
		std::stringstream	parse_line(line);

		if (!getline(parse_line, unique_id, ':'))
			throw std::exception();
		if (!getline(parse_line, nom, ':'))
			throw std::exception();
		if (!getline(parse_line, image_id, ':'))
			throw std::exception();
		if (!getline(parse_line, valeur_monetaire))
			throw std::exception();

		PDEBUG(2, "Uid : " + unique_id + " - Nom : " + nom + " - Imageid : " + image_id + " - ValMon : " + valeur_monetaire + " (" + line + ")");

		Objet::objets_base.push_back(Objet(unique_id, nom, stoi(image_id), stoi(valeur_monetaire)));
	}

	#if DEBUG_LEVEL >= 1
			for (auto obj : Objet::objets_base)
			{
				debug(obj.getUnique_id() + "|" + obj.getNom() + "|" + std::to_string(obj.get_index_text()) + "|" + std::to_string(obj.getValeur_monetaire()));
			}
		#endif

	PDEBUG(3, "File read ok !");
}

Objet				Objet::getObjetBaseByUniqueId(std::string unique_id)
{
	if (Objet::objets_base.empty())
		Objet::chargerObjetsBase();

	if (Objet::objets_base.empty())
		throw std::exception();

	for (auto obj : Objet::objets_base)
	{
		if (obj.getUnique_id() == unique_id)
			return Objet(obj.getUnique_id(), obj.getNom(), obj.get_index_text(), obj.getValeur_monetaire());
	}

	return Objet("ERROR", "Erreur : Objet inconnu", 0);
}

// Operator overloads

Objet				&Objet::operator=(Objet const &rhs)
{
	this->unique_id = rhs.getUnique_id();
	this->nom = rhs.getNom();
	this->valeur_monetaire = rhs.getValeur_monetaire();
	this->locked = rhs.getLocked();
	this->_index_text = rhs.get_index_text();

	return *this;
}

/*
std::ostream				&operator<<(std::ostream &stream, Objet const &item)
{
	//stream << item.toString();
	return stream;
}
*/
