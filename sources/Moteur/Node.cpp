#include "Node.hpp"

Node::Node() {}

Node::Node(Position const p, Position const arriver, Node *precedente) : p(p), precedente(precedente) 
{
	this->heuristique = Node::calc_heuristique(p, arriver);
	if (precedente)
		this->cost = precedente->cost + 1;
}

Node::Node(Node const & src) { *this = src;}

Node &	Node::operator=(Node const & rhs)
{
	this->p = rhs.p;
	this->heuristique = rhs.heuristique;
	this->precedente = rhs.precedente;
	this->cost = rhs.cost;

	return *this;
}
bool	Node::operator==(Node const & rhs) const
{
	if (this->p != rhs.p) return false;
	return true;
}
bool	Node::operator==(Position const & rhs) const {return (this->p == rhs) ? true : false;}
bool	Node::operator!=(Position const & rhs) const {return !(*this == rhs); }
bool	Node::operator!=(Node const & rhs) const {return !(*this == rhs); }
bool	Node::operator<(Node const & rhs) const {return (this->get_val() < rhs.get_val()) ? true : false ;}
bool	Node::operator>(Node const & rhs) const {return (this->get_val() > rhs.get_val()) ? true : false ;}

int			Node::get_heuristique()	const {return this->heuristique;}
int			Node::get_cost()		const {return this->cost;}
int			Node::get_val()			const {return this->heuristique + this->cost;}
Node *		Node::get_precedente()	const {return this->precedente;}
Position	Node::get_position()	const {return this->p;}


int		Node::calc_heuristique(Position const depart, Position const arriver)
{
	return abs(depart.get_x() - arriver.get_x()) + abs(depart.get_y() - arriver.get_y()) + abs(depart.get_z() - arriver.get_z());
}
