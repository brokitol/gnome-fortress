
#include "Liste_Taches.hpp"
#include <list>
#include <iterator>
#include "debug.hpp"

#define DEBUG_LEVEL 4

// Constructors
Liste_Taches::Liste_Taches(void)
{
	//std::cout << "Liste_Taches constructed !" << std::endl;
}

Liste_Taches::Liste_Taches(Liste_Taches const &src)
{
	*this = src;
	//std::cout << "Liste_Taches constructed !" << std::endl;
}

// Destructor
Liste_Taches::~Liste_Taches(void)
{
	//std::cout << "Liste_Taches destructed !" << std::endl;
}

// Accessors

std::list<Tache>		Liste_Taches::getTaches(void) const { return this->taches; }

bool					Liste_Taches::isFreeTache( void )
{
	std::list<Tache>::iterator it = this->taches.begin();
	std::list<Tache>::iterator end = this->taches.end();

	while (it != end) {
		if ((*it).getLocked() == false)
			return (true);
		it++;
	}
	return (false);
}

void					Liste_Taches::lockTache(unsigned long int unique_id)
{
	std::list<Tache>::iterator it = this->taches.begin();
	std::list<Tache>::iterator end = this->taches.end();

	while (it != end) {
		if ((*it).getUniqueId() == unique_id)
		{
			(*it).setLocked(true);
			break ;
		}
		it++;
	}
}

Tache 					Liste_Taches::getFreeTache( void )
{
	std::list<Tache>::iterator it = this->taches.begin();
	std::list<Tache>::iterator end = this->taches.end();

	while (it != end) {
		if ((*it).getLocked() == false)
			break ;
		it++;
	}
	return (*it);
}

void					Liste_Taches::setTaches(std::list<Tache> taches) { this->taches = taches; }

void					Liste_Taches::addTache(Tache tache)
{
	tache.setUniqueId(this->current_id);
	this->current_id++;
	this->taches.push_back(tache);
	PDEBUG(4, "Creation de la tache id '" + std::to_string(this->current_id - 1));
}

void					Liste_Taches::removeTache(unsigned long int unique_id)
{
	PDEBUG(4, "Task id '" + std::to_string(unique_id) + "' - removing...");
	std::list<Tache>::iterator it = this->taches.begin();

	while (it != this->taches.end())
	{
		if ((*it).getUniqueId() == unique_id)
		{
			PDEBUG(4, std::to_string(taches.size()) + "/");
			this->taches.erase(it);
			PDEBUG(4, std::to_string(taches.size()));
			PDEBUG(4, "Task id '" + std::to_string(unique_id) + "' removed !");
			break;
		}
		it++;
	}

}

// Operator overloads

Liste_Taches				&Liste_Taches::operator=(Liste_Taches const &rhs)
{
	this->taches = rhs.getTaches();

	return *this;
}

/*
std::ostream				&operator<<(std::ostream &stream, Liste_Taches const &item)
{
	//stream << item.toString();
	return stream;
}
*/

