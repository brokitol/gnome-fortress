/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/29 18:12:07 by bgauci            #+#    #+#             */
/*   Updated: 2015/06/29 18:49:10 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Controlleur.hpp"
#include <stdlib.h> // rand - srand

int	main()
{
	srand(time(NULL));
	Controlleur controlleur;
	controlleur.main();
	return 1;
}
