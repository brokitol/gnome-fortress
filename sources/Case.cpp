/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   case.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/02 20:17:16 by bgauci            #+#    #+#             */
/*   Updated: 2015/09/02 12:21:33 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Case.hpp"

Case::Case() {}

void	Case::set_sol(std::shared_ptr<Objet> obj)
{
	this->sol = obj;
	this->update.set_floor_flag();
}

void	Case::set_contenu(std::shared_ptr<Objet> obj)
{
	this->contenu = obj;
	this->update.set_content_flag();
}

std::shared_ptr<Objet>						Case::get_sol(void) const {return this->sol;}
std::shared_ptr<Objet>						Case::get_contenu(void) const {return this->contenu;}
std::list<std::shared_ptr<Entite_Mouvante>>	&Case::get_occupant(void) { return this->occupant; }
std::list<std::shared_ptr<Objet>>			&Case::get_divers(void) { return this->divers; }
Update										&Case::get_update(void) { return this->update; }

Case &	Case::operator+=(std::shared_ptr<Entite_Mouvante> obj)
{
	this->occupant.push_back(obj);
	this->update.set_character_flag();
	return *this;
}
Case &	Case::operator-=(std::shared_ptr<Entite_Mouvante> obj)
{
	this->occupant.remove(obj);
	this->update.set_character_flag();
	return *this;
}
Case &	Case::operator+=(std::shared_ptr<Objet> obj)
{
	this->divers.push_back(obj);
	this->update.set_object_flag();
	return *this;
}
Case &	Case::operator-=(std::shared_ptr<Objet> obj)
{
	this->divers.remove(obj);
	this->update.set_object_flag();
	return *this;
}


bool	Case::est_traversable(Entite_Mouvante const & toto) const {return toto.traverse(*this);}
