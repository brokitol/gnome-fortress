<?php

//-----------------------------//
// C++ class generator (alpha)
// by dbenoit
//-----------------------------//


// Templates

$hpp_template = "
#ifndef [ifndef]
# define [ifndef]

//# include <ostream>
//# include <string>

class [class_name]
{

private:
[attributes]
protected:

public:
	// Constructors
	[class_name](void);
	[class_name]([class_name] const &src);
	
	// Destructor
	~[class_name](void);
	
	// Accessors
[hpp_getters]
	
[hpp_setters]
	
	// Operator overloads
	[class_name]				&operator=([class_name] const &rhs);
};

// std::ostream				&operator<<(std::ostream &stream, [class_name] const &item);

#endif
";

$attributes_template = "	[type]				[name];";
$hpp_getters_template = "	[type]				get[accessor](void) const;";
$hpp_setters_template = "	void				set[accessor]([type] [name]);";


$cpp_template = "
#include \"[class_name].hpp\"

// Constructors
[class_name]::[class_name](void)
{
	//std::cout << \"[class_name] constructed !\" << std::endl;
}

[class_name]::[class_name]([class_name] const &src)
{
	*this = src;
	//std::cout << \"[class_name] constructed !\" << std::endl;
}

// Destructor
[class_name]::~[class_name](void)
{
	//std::cout << \"[class_name] destructed !\" << std::endl;
}

// Accessors

[cpp_getters]

[cpp_setters]

// Operator overloads

[class_name]				&[class_name]::operator=([class_name] const &rhs)
{
[equal_operator]
	return *this;
}

/*
std::ostream				&operator<<(std::ostream &stream, [class_name] const &item)
{
	//stream << item.toString();
	return stream;
}
*/

";

$cpp_getters_template = "[type]					[class_name]::get[accessor](void) const { return this->[name]; }";
$cpp_setters_template = "void					[class_name]::set[accessor]([type] [name]) { this->[name] = [name]; }";
$equal_operator_template = "	this->[name] = rhs.get[accessor]();";



// Script start


echo "Class name : ";

$class_name = rtrim(fgets(STDIN));

$attr = array();
echo "<attr>,<type> : ";
while (($line = rtrim(fgets(STDIN))) != "")
{
	$parts = explode(",", $line);
	$name_part = $parts[0];
	$type_part = $parts[1];
	$name = strtolower(implode("", explode(" ", $name_part)));
	$accessor = implode("", explode(" ", ucwords($name_part)));
	$type = $type_part;

	$attr[] = array("line" => $line, "name_part" => $name_part, "type_part" => $type_part, "name" => $name, "type" => $type, "accessor" => $accessor);
	echo "<attr>,<type> : ";
}


// HPP

$ifndef = strtoupper($class_name)."_HPP";

$attributes = "";
$hpp_getters = "";
$hpp_setters = "";
foreach($attr as $att)
{
	$temp_attribute = $attributes_template;
	$temp_attribute = str_replace("[type]", $att['type'], $temp_attribute);
	$temp_attribute = str_replace("[name]", $att['name'], $temp_attribute);
	
	$attributes .= $temp_attribute.PHP_EOL;
	
	$temp_getter = $hpp_getters_template;
	$temp_getter = str_replace("[type]", $att['type'], $temp_getter);
	$temp_getter = str_replace("[name]", $att['name'], $temp_getter);
	$temp_getter = str_replace("[accessor]", $att['accessor'], $temp_getter);
	
	$hpp_getters .= $temp_getter.PHP_EOL;
	
	$temp_setter = $hpp_setters_template;
	$temp_setter = str_replace("[type]", $att['type'], $temp_setter);
	$temp_setter = str_replace("[name]", $att['name'], $temp_setter);
	$temp_setter = str_replace("[accessor]", $att['accessor'], $temp_setter);
	
	$hpp_setters .= $temp_setter.PHP_EOL;
}


$hpp_template = str_replace("[ifndef]", $ifndef, $hpp_template);
$hpp_template = str_replace("[attributes]", $attributes, $hpp_template);
$hpp_template = str_replace("[hpp_getters]", $hpp_getters, $hpp_template);
$hpp_template = str_replace("[hpp_setters]", $hpp_setters, $hpp_template);

$hpp_template = str_replace("[class_name]", $class_name, $hpp_template);

$hpp = $hpp_template;


// CPP

$cpp_getters = "";
$cpp_setters = "";
$equal_operator = "";
foreach($attr as $att)
{
	$temp_getter = $cpp_getters_template;
	$temp_getter = str_replace("[type]", $att['type'], $temp_getter);
	$temp_getter = str_replace("[name]", $att['name'], $temp_getter);
	$temp_getter = str_replace("[accessor]", $att['accessor'], $temp_getter);
	
	$cpp_getters .= $temp_getter.PHP_EOL;
	
	$temp_setter = $cpp_setters_template;
	$temp_setter = str_replace("[type]", $att['type'], $temp_setter);
	$temp_setter = str_replace("[name]", $att['name'], $temp_setter);
	$temp_setter = str_replace("[accessor]", $att['accessor'], $temp_setter);
	
	$cpp_setters .= $temp_setter.PHP_EOL;
	
	$temp_equal_operator = $equal_operator_template;
	$temp_equal_operator = str_replace("[type]", $att['type'], $temp_equal_operator);
	$temp_equal_operator = str_replace("[name]", $att['name'], $temp_equal_operator);
	$temp_equal_operator = str_replace("[accessor]", $att['accessor'], $temp_equal_operator);
	
	$equal_operator .= $temp_equal_operator.PHP_EOL;
}

$cpp_template = str_replace("[attributes]", $attributes, $cpp_template);
$cpp_template = str_replace("[cpp_getters]", $cpp_getters, $cpp_template);
$cpp_template = str_replace("[cpp_setters]", $cpp_setters, $cpp_template);
$cpp_template = str_replace("[equal_operator]", $equal_operator, $cpp_template);

$cpp_template = str_replace("[class_name]", $class_name, $cpp_template);


$cpp = $cpp_template;

echo $hpp.PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL.$cpp;

if (isset($argv[1]) && $argv[1] == "-f")
{
	file_put_contents($class_name.".hpp", $hpp);
	file_put_contents($class_name.".cpp", $cpp);
}
else
{
	if (file_exists($class_name.".hpp"))
		file_put_contents($class_name.".hpp.gen", $hpp);
	else
		file_put_contents($class_name.".hpp", $hpp);

	if (file_exists($class_name.".cpp"))
		file_put_contents($class_name.".cpp.gen", $cpp);
	else
		file_put_contents($class_name.".cpp", $cpp);
}



?>