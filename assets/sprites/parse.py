import xml.etree.ElementTree as ET
import re

tree = ET.parse('sprites_without_body_parts.xml')
root = tree.getroot()

for child in root:
    name = child[0].text
    rect = child[1].text
    if len(child) == 3:
        offset = child[2].text
    else:
        offset = "0 0"
    if re.match("\d+ \d+ 32 32", rect) and offset != "0 -12":
        print("this->setTexture(img, sf::IntRect(" + rect.replace(" ", ", ") + "), sf::Vector2f(" + offset.replace(" ", ", ") + "));     // " + name)
